<?php

namespace Visualiseur\Core\Routers;

use Phalcon\Mvc\Router\Group;

/**
 * Class Main
 * Basic router
 *
 * @package Visualiseur\Core\Routers
 */
class Main extends Group
{
  /**
   * Admin home page
   */
  const ADMIN = 'coreMainHome';

  /**
   * Routes with only controllers
   */
  const CONTROLLER = 'coreMainController';

  /**
   * Routes with action and/or params
   */
  const ACTION = 'coreMainAction';

  /**
   * Main router definitions
   */
  public function initialize()
  {
    $this->setPaths([
      'module' => 'Core',
    ]);
    $this->setPrefix('/core');
    $this->add('')->setName(self::ADMIN);
    
    $this->add(
      '/:controller',
      ['controller' => 1]
    )->setName(self::CONTROLLER);

    $this->add(
      '/:controller/:action/:params',
      [
        'controller' => 1,
        'action'     => 2,
        'params'     => 3,
      ]
    )->setName(self::ACTION);
  }
}
