<?php

namespace Visualiseur\Core\Controllers;

/**
 * Class IndexController
 *
 * @package Visualiseur\Core\Controllers
 */
class IndexController extends ControllerBase
{
  /**
   * Index action
   */
  public function indexAction()
  {
    die(__FILE__.'<br/>'.__METHOD__.' ligne : '.__LINE__);
  }
}