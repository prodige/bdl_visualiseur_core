<?php

namespace Visualiseur\Core\Controllers;

use Visualiseur\Core\Controllers\Traits\HttpTrait;
use Visualiseur\Core\Controllers\Traits\RequestTrait;
use Visualiseur\Core\Controllers\Traits\ContextTrait;

/**
 * Class LocatorController
 *
 * @package Visualiseur\Core\Controllers
 */
class LocatorController extends ControllerBase
{

    /**
     * Modifie dynamiquement le .map pour rendre le service interrogreable en créant un .map temporaire
     * Effectue la requête sur le layer passé en paramètre puis renvoie le geojson
     * @param string account
     * @param string map
     * @param string locator
     * @param string wfs
     */
    public function queryLocatorAction($account, $map, $locator, $locateName, $optionName, $optionValue) {

        // Creation d'un .map temporaire
        $oMap = ms_newmapobj( $this->config->params->PATH_DATA . $account."Publication/". $locator . '_' . $map);

        // Modification du .map pour forcer le caractère interrogeable
        $oLayer = $oMap->getLayerByName($locateName);
        $oLayer->setMetaData('gml_include_items', 'all');
        $oMap->save("temporaire.map");

        // Requêter avec le wfs sur le layer concerné retrouvé grâce au locator
        /* $data = '<?xml version="1.0" encoding="UTF-8"?> <GetFeature service="WFS" version="2.0.0" count="100" outputformat="geojson" '.
                    'xmlns="http://www.opengis.net/wfs/2.0" xmlns:cw="http://www.someserver.com/cw" xmlns:fes="http://www.opengis.net/ogc/1.1" '.
                    'xmlns:gml="http://www.opengis.net/gml/3.2" > <Query typeNames="'.$layerName.'" propertyName="'.$key.'"  styles="default"><Filter>'.
                    '<PropertyIsEqualTo><PropertyName>'.$optionName.'</PropertyName><Literal>'.$optionValue.'</Literal></PropertyIsEqualTo></Filter></Query></GetFeature>'; */


        // Renvoyer le geojson de réponse

    }
    
}