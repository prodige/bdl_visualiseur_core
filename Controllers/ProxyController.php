<?php

namespace Visualiseur\Core\Controllers;
use Phalcon\Http\Request;
use Phalcon\Http\Client\Exception;

use Phalcon\Mvc\Url;

/**
 * Class ProxyController
 *
 * @package Visualiseur\Core\Controllers
 */
class ProxyController extends ControllerBase
{

  protected static $httpStatusCodes = array(
    100 => "Continue", 101 => "Switching Protocols", 102 => "Processing", 
    200 => "OK", 201 => "Created", 202 => "Accepted", 203 => "Non-Authoritative Information", 204 => "No Content", 205 => "Reset Content", 206 => "Partial Content", 207 => "Multi-Status", 
    300 => "Multiple Choices", 301 => "Moved Permanently", 302 => "Found", 303 => "See Other", 304 => "Not Modified", 305 => "Use Proxy", 306 => "(Unused)", 307 => "Temporary Redirect", 308 => "Permanent Redirect", 
    400 => "Bad Request", 401 => "Unauthorized", 402 => "Payment Required", 403 => "Forbidden", 404 => "Not Found", 405 => "Method Not Allowed", 406 => "Not Acceptable", 407 => "Proxy Authentication Required", 408 => "Request Timeout", 409 => "Conflict", 410 => "Gone", 411 => "Length Required", 412 => "Precondition Failed", 413 => "Request Entity Too Large", 414 => "Request-URI Too Long", 415 => "Unsupported Media Type", 416 => "Requested Range Not Satisfiable", 417 => "Expectation Failed", 418 => "I'm a teapot", 419 => "Authentication Timeout", 420 => "Enhance Your Calm", 422 => "Unprocessable Entity", 423 => "Locked", 424 => "Failed Dependency", 424 => "Method Failure", 425 => "Unordered Collection", 426 => "Upgrade Required", 428 => "Precondition Required", 429 => "Too Many Requests", 431 => "Request Header Fields Too Large", 444 => "No Response", 449 => "Retry With", 450 => "Blocked by Windows Parental Controls", 451 => "Unavailable For Legal Reasons", 494 => "Request Header Too Large", 495 => "Cert Error", 496 => "No Cert", 497 => "HTTP to HTTPS", 499 => "Client Closed Request", 
    500 => "Internal Server Error", 501 => "Not Implemented", 502 => "Bad Gateway", 503 => "Service Unavailable", 504 => "Gateway Timeout", 505 => "HTTP Version Not Supported", 506 => "Variant Also Negotiates", 507 => "Insufficient Storage", 508 => "Loop Detected", 509 => "Bandwidth Limit Exceeded", 510 => "Not Extended", 511 => "Network Authentication Required", 598 => "Network read timeout error", 599 => "Network connect timeout error"
    );


  /**
   * Proxy action
   */
  public function indexAction()
  {
    $proxyUrl = $this->request->getQuery('proxyUrl', null);    

    if (filter_var($proxyUrl, FILTER_VALIDATE_URL) === FALSE) {
      $this->sendHttpError("500", "Invalid URL");
      return;
    }
    $params = array();

    $urlElts = parse_url($proxyUrl);
    if(isset($urlElts['query'])){
        parse_str($urlElts['query'], $params);
    }
    $baseUrl = $urlElts["scheme"]."://".
               (isset($urlElts["user"]) ? $urlElts["user"] : "").
               (isset($urlElts["pass"]) ? ":".$urlElts["pass"]."@" : "").
               $urlElts["host"].
               (isset($urlElts["port"]) ? ":".$urlElts["port"] : "").
               (isset($urlElts["path"]) ? $urlElts["path"] : "");

    $method = ($this->request->isPost() ? "POST" : "GET");
    $this->loadUrl($baseUrl, $params, $method);
  }

  /**
   * Chargement de l'URL selon la méthode définie et les paramètres passés
   * 
   */
  protected function loadUrl($url, array $urlParams, $method='GET', $logLevel=6, $streamingMode=true, $bufferSize=65536, $timeout=30, $stdout='', $killSessionAfter=false)
    {
        $done = false;
        $this->fStdOut = null;
        $this->curl = null;
        $headers = array();
        
        $this->statLoading = array("totalSize" => 0, "uploadedSize" => 0);
        
        // ouverture en écriture de sortie standard
        if( !empty($stdout) ) {
          $this->fStdOut = fopen($stdout, 'wb');
        } 
        
        try {            
          
            $this->curl = curl_init();
            if( !is_resource($this->curl) ) {
              throw new \Exception("Unable to init curl connexion : ", 500);
            }

            // lecture du contenu de l'url et redirection vers la sortie standard
            $this->getContentFromUrl($url, $urlParams, $method, $bufferSize, $timeout, $streamingMode);
            $done = true;
            
        } catch( \Exception $e ) {
          $code = $e->getCode();
          $this->sendHttpError($code, "");
        }
        
        if( is_resource($this->fStdOut) ) {
          fclose($this->fStdOut);
        }
        $this->fStdOut = null;
        
        if( is_resource($this->curl) ) {
          curl_close($this->curl);
        }
        $this->curl = null;
        
        exit();
    }
    
    
  
    /**
     * Ecrit data sur la sortie standard
     * @param string $data
     */
    protected function writeToStandardOutput($data)
    {
      if( isset($this->fStdOut) && is_resource($this->fStdOut) ) {
        fwrite($this->fStdOut, $data);
      } else {
        echo $data;
      }
    }

    /**
     * Envoi une erreur http avec le message d'erreur
     * @param int    $httpCode
     * @param string $error
     */
    protected function sendHttpError($httpCode=0, $error="")
    {
        $httpCode = ( isset(self::$httpStatusCodes[$httpCode]) ? $httpCode : 500 );
        if( !headers_sent() ) {
            header($_SERVER['SERVER_PROTOCOL']." ".$httpCode." ".self::$httpStatusCodes[$httpCode], true, $httpCode);
        }
        if( !empty($error) ) {
            $this->writeToStandardOutput($error);
        }
    }
    
    /**
     * Envoi le header avec le report des informations reçues par curl
     * Retourne true si le header de code 200 a été envoyé, false sinon (cas où il a déjà été envoyé)
     * @param array  $info   curl info
     * @return bool
     */
    protected function sendHttpHeader($info)
    {
        if( headers_sent() ) {
            
            return false;
        }
      
        $httpCode    = ( isset($info["http_code"]) ? $info["http_code"] : 200 );
        $contentType = ( isset($info["content_type"]) ? $info["content_type"] : "text/html; charset=UTF-8" );
        
        if( substr($contentType, 0, 9) == "text/html" ) {
            // les retours attendus sont soit : json, xml ou images
            header($_SERVER['SERVER_PROTOCOL']." 500 ".self::$httpStatusCodes[500], true, 500);
            return false;
        }
        
        $fileName    = md5($info["url"]);
        $fileExt     = explode('/', explode(";", $contentType)[0] )[1];
        
        header(sprintf("%s %s %s", $_SERVER['SERVER_PROTOCOL'], $httpCode, self::$httpStatusCodes[$httpCode]), true, $httpCode);
        header(sprintf("content-type: %s", $contentType));
        
        return true;
    }
    /**
     * Fonction de retour asynchrone pour écrire le résultat sur la sortie standard
     * @param string $curl
     * @param string $data
     * @param int
     */
    protected function curlCallback($curl, $data) 
    {
        if( $this->statLoading["uploadedSize"] == 0 ) {
          // envoi le header
          $info = curl_getinfo($curl);
          $sent200 = $this->sendHttpHeader($info);

          if( !$sent200 ) {
            throw new \Exception($data, 404);
          }
        }
        $length = strlen($data);
        
        $this->statLoading["uploadedSize"] += $length;
       
        if( is_resource($this->fStdOut) ) {
          fwrite($this->fStdOut, $data);
          fflush($this->fStdOut);
        } else {
          echo $data;
          flush();
        }
        // le transfert s'arretera lorsque la longueur 0 sera retournée
        return $length;
    }
  
    /**
     * Charge le contenu du lien fourni selon la méthode sélectionnée
     * Nécessite que la connexion curl soit initialisée
     * Retourne true ou lève une exception
     * @param string    $url           url 
     * @param array     $urlParams     paramètres de l'url
     * @param string    $method        =POST|GET|PUT|...
     * @param int       $bufferSize    taille de buffer pour la lecture du contenu
     * @param int       $timeout       temps en seconde pour libérer la connexion en cas de non réponse
     * @param bool      $streamingMode =true pour un chargement en streaming, =false pour une lecture du flux en 1 fois
     * @throws \Exception
     */
    protected function getContentFromUrl($url, array $urlParams, $method, $bufferSize, $timeout, $streamingMode)
    {
        curl_reset($this->curl);
        
        $params = http_build_query($urlParams);

        if($params){
            $url .= '?'.$params;
        }
        
        
        $headers = array();
        
        foreach (getallheaders() as $name => $value) {
            if(strtolower($name)!=="accept-encoding" && strtolower($name)!=="host"){
                $headers[] = $name.": ".$value;
            }
        }
        
        $optEnabled =
          curl_setopt($this->curl, CURLOPT_URL, $url)
          && curl_setopt($this->curl, CURLOPT_POST, $method=='POST')
          && curl_setopt($this->curl, CURLOPT_PUT, $method=='PUT') 
          //&& curl_setopt($this->curl, CURLOPT_BUFFERSIZE, $bufferSize)
          && curl_setopt($this->curl, CURLOPT_BINARYTRANSFER, true)
          && curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, !is_resource($this->fStdOut))
          && curl_setopt($this->curl, CURLOPT_FRESH_CONNECT, false)
          && curl_setopt($this->curl, CURLOPT_HEADER, false)      
          && curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true)
          && curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers)
          && curl_setopt($this->curl, CURLOPT_TIMEOUT, $timeout)
          && ( $method === 'GET'  || curl_setopt($this->curl, CURLOPT_POSTFIELDS, file_get_contents('php://input')) ) // send request body
          && ( !$streamingMode || $streamingMode && curl_setopt($this->curl, CURLOPT_WRITEFUNCTION, array($this, 'curlCallback')))
         ;

        $proxyParams["url"] = isset($this->config->params->PROXY_PARAM_URL) ? $this->config->params->PROXY_PARAM_URL : "";
        $proxyParams["port"] = isset($this->config->params->PROXY_PARAM_PORT) ? $this->config->params->PROXY_PARAM_PORT : "";
        
        if($proxyParams["url"]!=""){
            $optEnabled = $optEnabled && curl_setopt($this->curl, CURLOPT_PROXY, $proxyParams["url"].":".$proxyParams["port"]);
        }
        if( !$optEnabled ) {
          throw new \Exception(sprintf("Unable to parameter curl connexion : [%s] %s", curl_errno($this->curl), curl_error($this->curl)), 500);
        }

        $ok = curl_exec($this->curl);
        
        
        if( !$ok ) {
          throw new \Exception(sprintf("1- Unable to read url %s with method %s : [%s] %s", $url, $method, curl_errno($this->curl), curl_error($this->curl)), 404);
        } elseif( !$streamingMode ) {
          $info = curl_getinfo($this->curl);
          $sent200 = $this->sendHttpHeader($info);
          if( !$sent200 ) {
            throw new \Exception(sprintf("2- Unable to read url %s with method %s : [%s] %s", $url, $method, curl_errno($this->curl), curl_error($this->curl)), 404);
          }
          echo $ok;
          flush();
        }
    }
}