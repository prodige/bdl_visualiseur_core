<?php

namespace Visualiseur\Core\Controllers;

/**
 * Class ExportannotationController
 *
 * @package Visualiseur\Core\Controllers
 */
class ExportannotationController extends ControllerBase
{

    public function indexAction()
    {
        $response = new \Phalcon\Http\Response();
        try {
            if (!$this->request->isPost()) {
                $response->setJsonContent([
                    "success" => false,
                    "message" => "Post Request is mandatory.",
                ]);
                return $response;
            }

            $json = json_decode($this->request->getRawBody(), true);

            if (is_null($json)) {
                throw new \Exception("Failed to parse JSON. Content length is " . strlen($this->request->getRawBody()));
            }

            if (!isset($json["geojson"])) {
                throw new \Exception("parameter geojson is missing.");
            }
            if (!isset($json["type"])) {
                throw new \Exception("parameter type is missing.");
            }

            $annotations = $json["geojson"];
            $jsonAnnotations = json_encode($annotations);

            $filePrefix = "export_annotations_" . uniqid();
            $filePathGeoJson = $this->config->params->PATH_DATA .$this->config->params->MAPIMAGE_DIR. "/" . $filePrefix . ".geojson";
            file_put_contents($filePathGeoJson, $jsonAnnotations);

            
            $output = array();
            $return = null;

            if (strcasecmp(strtolower($json["type"]), "shp") == 0) {
                $polygonGeojson = null;
                $linestringGeojson = null;
                $pointGeojson = null;

                foreach($annotations["features"] as $feature){
                    switch ($feature["geometry"]["type"]){
                        case "LineString":
                            if($linestringGeojson===null){
                                $linestringGeojson = $annotations;
                                //remove all features
                                $linestringGeojson["features"] = array();
                            }
                            array_push($linestringGeojson["features"], $feature);
                        break;
                        case "Polygon":
                            if($polygonGeojson===null){
                                $polygonGeojson = $annotations;
                                //remove all features
                                $polygonGeojson["features"] = array();
                            }
                            array_push($polygonGeojson["features"], $feature);
                        break;
                        case "Point":
                            if($pointGeojson===null){
                                $pointGeojson = $annotations;
                                //remove all features
                                $pointGeojson["features"] = array();
                            }
                            array_push($pointGeojson["features"], $feature);
                        break;
                    }
                }

                
                $filePathPattern = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/". $filePrefix . "_*";
                $filePathToReturn = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/". $filePrefix . "_shape.zip";
                $ogr2ogrFormat = "'ESRI Shapefile'";
                $mimeTypeToReturn = "application/zip";

                if($polygonGeojson !== null){
                    $filePathGeoJson = $this->config->params->PATH_DATA .$this->config->params->MAPIMAGE_DIR. "/" . $filePrefix . "polygon.geojson";
                    file_put_contents($filePathGeoJson, json_encode($polygonGeojson));
                    $filePathTarget = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/" . $filePrefix . "_polygon.shp";
                    $cmd = "ogr2ogr -f " . $ogr2ogrFormat . " " . $filePathTarget . " " . $filePathGeoJson;
                    $result = exec($cmd, $output, $return);
                    if ($result != 0) {
                        throw new \Exception("ogr2ogr returned an error");
                    }
                }
                if($linestringGeojson !== null){
                    $filePathGeoJson = $this->config->params->PATH_DATA .$this->config->params->MAPIMAGE_DIR. "/" . $filePrefix . "line.geojson";
                    file_put_contents($filePathGeoJson, json_encode($linestringGeojson));
                    $filePathTarget = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/" . $filePrefix . "_line.shp";
                    $cmd = "ogr2ogr -f " . $ogr2ogrFormat . " " . $filePathTarget . " " . $filePathGeoJson;
                    $result = exec($cmd, $output, $return);
                    if ($result != 0) {
                        throw new \Exception("ogr2ogr returned an error");
                    }
                }
                if($pointGeojson !== null){
                    $filePathGeoJson = $this->config->params->PATH_DATA .$this->config->params->MAPIMAGE_DIR. "/" . $filePrefix . "point.geojson";
                    file_put_contents($filePathGeoJson, json_encode($pointGeojson));
                    $filePathTarget = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/" . $filePrefix . "_point.shp";
                    $cmd = "ogr2ogr -f " . $ogr2ogrFormat . " " . $filePathTarget . " " . $filePathGeoJson;
                    $result = exec($cmd, $output, $return);
                    if ($result != 0) {
                        throw new \Exception("ogr2ogr returned an error");
                    }
                }
                                
                $extraCmdZip = "zip -j " . $filePathToReturn . " " . $filePathPattern;
            } else if (strcasecmp(strtolower($json["type"]), "kml") == 0) {
                
                $filePathTarget = $this->config->params->PATH_DATA .$this->config->params->MAPIMAGE_DIR. "/". $filePrefix . ".kml";
                $filePathToReturn = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/". $filePrefix . ".kml";
                $ogr2ogrFormat = "'KML'";
                $mimeTypeToReturn = "application/vnd.google-earth.kml+xml";
                
                $cmd = "ogr2ogr -f " . $ogr2ogrFormat . " " . $filePathTarget . " " . $filePathGeoJson;

                $result = exec($cmd, $output, $return);
                if ($result != 0) {
                    throw new \Exception("ogr2ogr returned an error");
                }
                $extraCmdZip = "";
                                
            } else {
                throw new \Exception("parameter type is invalid.");
            }

            

            // ogr2ogr -f 'ESRI Shapefile'  export_annotations_5d482c355b240.shp export_annotations_5d482c355b240.geojson

            if (strcmp($extraCmdZip, "") != 0) {
                $result = exec($extraCmdZip, $output, $return);
                if ($result != 0) {
                    throw new \Exception("zip returned an error");
                }
            }

            $fileContent = file_get_contents($filePathToReturn);

//            $response->setHeader('Content-Transfer-Encoding', 'binary');// looks not conform to standard - that header is for email
//            $response->setHeader('Content-Type', 'application/force-download');// It's not a valid MIME type
            $response->setHeader('Content-Type', $mimeTypeToReturn);
            $response->setHeader('Content-Disposition', "attachment; filename=" . basename($filePathToReturn) . "");

            $response->setContent($fileContent);

            return $response;
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to export annotations. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

}
