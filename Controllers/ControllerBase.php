<?php

namespace Visualiseur\Core\Controllers;

use Phalcon\Mvc\Controller;
use Visualiseur\Core\Controllers\Traits\RequestTrait;

abstract class ControllerBase extends Controller
{
    use RequestTrait;


    /**
     * Retourne le chemin complet d'accès aux données du service connecté
     * @return string
     */
    public function getPathToMapfileDirectory()
    {
        $rootPath = $this->config->params->PRODIGE_PATH_DATA;
      
    	return $rootPath.'/cartes/Publication';
    }

}