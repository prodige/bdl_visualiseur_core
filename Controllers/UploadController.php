<?php

namespace Visualiseur\Core\Controllers;

use Phalcon\Http\Client\Exception;
use Phalcon\Http\Request;
use Phalcon\Mvc\Url;

use Visualiseur\Core\Controllers\Traits\ContextTrait;

/**
 * Class UploadController
 *
 * @package Visualiseur\Core\Controllers
 */
class UploadController extends ControllerBase
{

    use ContextTrait;

    public function uploadAction()
    {
       
        // test commit sur la nouvelle branche
        $this->view->disable();
        $response = new \Phalcon\Http\Response();
        try {
            if (!$this->request->isPost()) {
                $response->setJsonContent([
                    "success" => false,
                    "message" => "Post Request is mandatory.",
                ]);
                return $response;
            }

            $account = $this->request->get("account");
            $accountPath = $this->getServicePath($account);
            
            if ($this->request->hasFiles()) {
                $files = $this->request->getUploadedFiles();
                
                foreach ($files as $file) {
          
                    if (strtolower($file->getExtension()) == "cpg") { // shape
                        $cpgFile = $file;
                    } elseif (strtolower($file->getExtension()) == "dbf") {
                        $dbfFile = $file;
                    } elseif (strtolower($file->getExtension()) == "prj") {
                        $prjFile = $file;
                    } elseif (strtolower($file->getExtension()) == "shp") {
                        $shpFile = $file;
                    } elseif (strtolower($file->getExtension()) == "shx") {
                        $shxFile = $file;
                    } elseif (strtolower($file->getExtension()) == "dat") { // tab
                        $datFile = $file;
                    } elseif (strtolower($file->getExtension()) == "id") {
                        $idFile = $file;
                    } elseif (strtolower($file->getExtension()) == "map") {
                        $mapFile = $file;
                    } elseif (strtolower($file->getExtension()) == "tab") {
                        $tabFile = $file;
                    } elseif (strtolower($file->getExtension()) == "mif") { // mif mid
                        $mifFile = $file;
                    } elseif (strtolower($file->getExtension()) == "mid") {
                        $midFile = $file;
                    } elseif (strtolower($file->getExtension()) == "kml") { // kml
                        $kmlFile = $file;
                    } elseif (strtolower($file->getExtension()) == "json") { // geojson     
                        $jsonFile = $file;
                    } elseif (strtolower($file->getExtension()) == "geojson") {
                        $jsonFile = $file;
                    } elseif (strtolower($file->getExtension()) == "csv") { // csv
                        $csvFile = $file;
                    }
                }
            }

            if (isset($shpFile)) {
                if (!isset($dbfFile) || !isset($prjFile) || !isset($shpFile) || !isset($shxFile)) {
                    throw new \Exception("shape format - files set is incomplete.");
                }
            } elseif (isset($tabFile)) {
                if (!isset($datFile) || !isset($idFile) || !isset($mapFile) || !isset($tabFile)) {
                    throw new \Exception("tab format - files set is incomplete.");
                }
            } elseif (isset($mifFile)) {
                if (!isset($mifFile) || !isset($midFile)) {
                    throw new \Exception("mif format - files set is incomplete.");
                }
            } elseif (isset($kmlFile)) {
                // kml format - no control
            } elseif (isset($jsonFile)) {
                // geojson format - no control
            } elseif (isset($csvFile)) {
                // csv format - no control
            } else {
                throw new \Exception("file format is not recognized.");
            }

            if (is_null($this->request->get("extentMinx")) || is_null($this->request->get("extentMiny")) || is_null($this->request->get("extentMaxx")) || is_null($this->request->get("extentMaxy"))) {
                throw new \Exception("extent data is missing or incomplete");
            }
            if (is_null($this->request->get("srsMap"))) {
                throw new \Exception("srsMap is missing");
            }

            //$uniqueId = time();
            $uniqueId = uniqid();

            
            if(!file_exists($this->config->params->PATH_DATA . $accountPath."Publication/".  "wfs/temp/")){
                mkdir($this->config->params->PATH_DATA . $accountPath."Publication/".  "wfs/temp/", 0755);
            }

            $targetDirectory = $this->config->params->PATH_DATA . $accountPath."Publication/".  "wfs/temp/" . $uniqueId . "/";
            
            mkdir($targetDirectory, 0755);

            if (isset($shpFile)) {
                if (isset($cpgFile)) {
                    $cpgFile->moveTo($targetDirectory . $this->sanitizeFilename($cpgFile->getName()));
                }
                $dbfFile->moveTo($targetDirectory . $this->sanitizeFilename($dbfFile->getName()));
                $prjFile->moveTo($targetDirectory . $this->sanitizeFilename($prjFile->getName()));
                $shpFile->moveTo($targetDirectory . $this->sanitizeFilename($shpFile->getName()));
                $shxFile->moveTo($targetDirectory . $this->sanitizeFilename($shxFile->getName()));
                $mainFilename = $this->sanitizeFilename($shpFile->getName());
            } elseif (isset($tabFile)) {
                $datFile->moveTo($targetDirectory . $this->sanitizeFilename($datFile->getName()));
                $idFile->moveTo($targetDirectory . $this->sanitizeFilename($idFile->getName()));
                $mapFile->moveTo($targetDirectory . $this->sanitizeFilename($mapFile->getName()));
                $tabFile->moveTo($targetDirectory . $this->sanitizeFilename($tabFile->getName()));
                $mainFilename = $this->sanitizeFilename($tabFile->getName());
            } elseif (isset($mifFile)) {
                $mifFile->moveTo($targetDirectory . $this->sanitizeFilename($mifFile->getName()));
                $midFile->moveTo($targetDirectory . $this->sanitizeFilename($midFile->getName()));
                $mainFilename = $this->sanitizeFilename($mifFile->getName());
            } elseif (isset($kmlFile)) {
                $kmlFile->moveTo($targetDirectory . $this->sanitizeFilename($kmlFile->getName()));
                $mainFilename = $this->sanitizeFilename($kmlFile->getName());
            } elseif (isset($jsonFile)) {
                $jsonFile->moveTo($targetDirectory . $this->sanitizeFilename($jsonFile->getName()));
                $mainFilename = $this->sanitizeFilename($jsonFile->getName());
                //for json upload, directly return geojson when it's necessary
                $isToEditing = $this->request->get("to_editing");
                $isToEditing = intval($isToEditing) ? true : false;
                if ($isToEditing){
                    return $response->setJsonContent(
                        array('data' => json_decode(file_get_contents($targetDirectory.$mainFilename), true), 
                              'type' => 'json', 
                              'success' => true));
                }
                

            } elseif (isset($csvFile)) {
                $csvFile->moveTo($targetDirectory . $this->sanitizeFilename($csvFile->getName()));
                $mainFilename = $this->sanitizeFilename($csvFile->getName());
            }


            //$cmd = "ogrinfo -ro -al -so -nocount -noextent -nomd " . $targetDirectory . $mainFilename;

            $layerName = pathinfo($mainFilename, PATHINFO_FILENAME);
            $cmd = 'ogrinfo -ro -geom=SUMMARY -dialect SQLite -sql "select GEOMETRY from \"'.$layerName.'\" limit 1" '.$targetDirectory . $mainFilename;
            
            $output = array();
            $return = null;
            $result = exec($cmd, $output, $return);

            if ($return != 0) {
                if (isset($shpFile)) {
                    if (isset($cpgFile)) {
                        unlink($targetDirectory . $this->sanitizeFilename($cpgFile->getName()));
                    }
                    unlink($targetDirectory . $this->sanitizeFilename($dbfFile->getName()));
                    unlink($targetDirectory . $this->sanitizeFilename($prjFile->getName()));
                    unlink($targetDirectory . $this->sanitizeFilename($shpFile->getName()));
                    unlink($targetDirectory . $this->sanitizeFilename($shxFile->getName()));
                } elseif (isset($tabFile)) {
                    unlink($targetDirectory . $this->sanitizeFilename($datFile->getName()));
                    unlink($targetDirectory . $this->sanitizeFilename($idFile->getName()));
                    unlink($targetDirectory . $this->sanitizeFilename($mapFile->getName()));
                    unlink($targetDirectory . $this->sanitizeFilename($tabFile->getName()));
                } elseif (isset($mifFile)) {
                    unlink($targetDirectory . $this->sanitizeFilename($mifFile->getName()));
                    unlink($targetDirectory . $this->sanitizeFilename($midFile->getName()));
                } elseif (isset($kmlFile)) {
                    unlink($targetDirectory . $this->sanitizeFilename($kmlFile->getName()));
                } elseif (isset($jsonFile)) {
                    unlink($targetDirectory . $this->sanitizeFilename($jsonFile->getName()));
                } elseif (isset($csvFile)) {
                    unlink($targetDirectory . $this->sanitizeFilename($csvFile->getName()));
                }
                rmdir($targetDirectory);
                throw new \Exception("reading file is producing an error.");
            }

            $columns = $this->getColumnsFromOgrinfoOutput($output);
            $geometryOgrinfo = $this->getFirstGeometryFromOgrinfoOutput($output);
            $srsInfo = $this->getSrsFromOgrinfoOutput($output);

            if (isset($csvFile)) {
                $response->setJsonContent([
                    "uniqueId" => $uniqueId,
                    "filename" => $mainFilename,
                    "columns" => $columns,
                ]);
                return $response;
            }

            // MS_LAYER_POINT, MS_LAYER_LINE, MS_LAYER_POLYGON, MS_LAYER_RASTER, MS_LAYER_ANNOTATION (deprecated since 6.2), MS_LAYER_QUERY, MS_LAYER_CIRCLE, MS_LAYER_TILEINDEX, MS_LAYER_CHART
            // from MapfileController _getGeometryType
            
            if ($geometryOgrinfo && stripos($geometryOgrinfo, 'LINESTRING') !== false) {
                $geometry = array("layerType" => MS_LAYER_LINE, "msGeometryType" => "LINE");
            } elseif ($geometryOgrinfo && stripos($geometryOgrinfo, 'POLYGON') !== false) {
                $geometry = array("layerType" => MS_LAYER_POLYGON, "msGeometryType" => "POLYGON");
            } elseif ($geometryOgrinfo && stripos($geometryOgrinfo, 'POINT') !== false) {
                $geometry = array("layerType" => MS_LAYER_POINT, "msGeometryType" => "POINT");
            } else {
                $geometry = array("layerType" => MS_LAYER_POINT, "msGeometryType" => "POINT");
            }

            if (isset($shpFile)) {
                $mapDataConnection = "data";
            } else {
                $mapDataConnection = "connection";
            }

            return $this->createMapFile($accountPath, $targetDirectory, $uniqueId, $mainFilename, $columns, $geometryOgrinfo, $geometry["layerType"], $mapDataConnection, 
                                        $this->request->get("extentMinx"), $this->request->get("extentMiny"), $this->request->get("extentMaxx"), $this->request->get("extentMaxy"), 
                                        $this->request->get("srsMap"), $srsInfo !="" ? $srsInfo : $this->request->get("srsMap"), null, null);

        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to process file(s). " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function addToServiceWfsAction()
    {
        $this->view->disable();
        $response = new \Phalcon\Http\Response();
        try {
            if (is_null($this->request->get("uniqueId"))) {
                throw new \Exception("uniqueId is missing.");
            }
            if (is_null($this->request->get("filename"))) {
                throw new \Exception("filename is missing.");
            }
            if (is_null($this->request->get("extentMinx")) || is_null($this->request->get("extentMiny")) || is_null($this->request->get("extentMaxx")) || is_null($this->request->get("extentMaxy"))) {
                throw new \Exception("extent data is missing or incomplete.");
            }
            if (is_null($this->request->get("srsLayer"))) {
                throw new \Exception("srsLayer is missing.");
            }
            if (is_null($this->request->get("srsMap"))) {
                throw new \Exception("srsMap is missing.");
            }
            if (is_null($this->request->get("colx")) || is_null($this->request->get("coly"))) {
                throw new \Exception("colx or coly is missing.");
            }

            $uniqueId = $this->request->get("uniqueId");
            $filename = $this->request->get("filename");
            $colx = $this->request->get("colx");
            $coly = $this->request->get("coly");

            $targetDirectory =  $this->config->params->PATH_DATA . $accountPath."Publication/" . "wfs/temp/" . $uniqueId . "/";
            if (!file_exists($targetDirectory)) {
                throw new \Exception("uniqueId is invalid.");
            }
            if (!file_exists($targetDirectory . $filename)) {
                throw new \Exception("filename is invalid.");
            }

            $cmd = "ogrinfo -ro -al -so -nocount -noextent -nomd " . $targetDirectory . $filename;
            $output = array();
            $return = null;
            $result = exec($cmd, $output, $return);
            $columns = $this->getColumnsFromOgrinfoOutput($output);
            $geometryOgrinfo = $this->getFirstGeometryFromOgrinfoOutput($output); // None

            if (!array_key_exists($colx, $columns)) {
                throw new \Exception("column x is absent from file.");
            }
            if (!array_key_exists($coly, $columns)) {
                throw new \Exception("column y is absent from file.");
            }

            $domvrt = new \DomDocument("1.0", "UTF-8");
            $domvrt->formatOutput = true;

            $elemDataSource = $domvrt->createElement('OGRVRTDataSource');
            $domvrt->appendChild($elemDataSource);

            $elemLayer = $domvrt->createElement('OGRVRTLayer');
            $elemLayer->setAttribute('name', substr($filename, 0, -4)); // remove extension, upper or lower case is unknown
            $elemDataSource->appendChild($elemLayer);

            $elemSrcDataSource = $domvrt->createElement('SrcDataSource', './' . $filename);
            $elemSrcDataSource->setAttribute('relativeToVRT', 1);
            $elemLayer->appendChild($elemSrcDataSource);
            $elemGeometryType = $domvrt->createElement('GeometryType', 'wkbPoint');
            $elemLayer->appendChild($elemGeometryType);
            $elemLayerSRS = $domvrt->createElement('LayerSRS', $this->request->get("srsLayer"));
            $elemLayer->appendChild($elemLayerSRS);
            $elemGeometryField = $domvrt->createElement('GeometryField');
            $elemGeometryField->setAttribute('encoding', 'PointFromColumns');
            $elemGeometryField->setAttribute('x', $colx);
            $elemGeometryField->setAttribute('y', $coly);
            $elemLayer->appendChild($elemGeometryField);

            $domvrt->save($targetDirectory . $uniqueId . '.vrt');

            return $this->createMapFile($targetDirectory, $uniqueId, $uniqueId . '.vrt', $columns, $geometryOgrinfo, MS_LAYER_POINT, 'connection', 
                                        $this->request->get("extentMinx"), $this->request->get("extentMiny"), $this->request->get("extentMaxx"), $this->request->get("extentMaxy"), 
                                        $this->request->get("srsMap"), $this->request->get("srsLayer"), $colx, $coly);
        } catch (\Exception $e) {
            //            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to process wfs for csv. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    protected function createMapFile($accountPath, $targetDirectory, $uniqueId, $filename, $columns, $geometryOgrinfo, $mapGeometry, 
                                    $mapDataConnection, $extentMinx, $extentMiny, $extentMaxx, $extentMaxy, $srsMap, $srsLayer, $colx, $coly)
    {
        $response = new \Phalcon\Http\Response();
        try {
            $oMap = ms_newmapobj( $this->config->params->PATH_DATA . $accountPath."Publication/". "/wfs/default.map");

            $oMap->name = $uniqueId;

            $oMap->setExtent($extentMinx, $extentMiny, $extentMaxx, $extentMaxy);
            
            $oMap->setProjection($srsMap);
            $oMap->setMetaData('ofs_srs', $srsMap);
            $oMap->setMetadata('wfs_onlineresource', $this->config->params->PRODIGE_URL_DATACARTO . "/map/wfs/temp/" . $uniqueId."/".$uniqueId);

            $oLayer = ms_newLayerObj($oMap);
            $oLayer->set('name', "temp_" . $uniqueId); //should not start with digit
            $oLayer->set('type', $mapGeometry);
            $oLayer->set('status', MS_ON);
            $oLayer->setProjection($srsLayer);
            $oLayer->setMetaData('gml_include_items', 'all');
            $oLayer->setMetaData('ows_title', "temp_" . $uniqueId);
            if ($mapDataConnection == "data") {
                $oLayer->set('data', $filename);
            } else {
                $oLayer->set('connection', $filename);
                $oLayer->setConnectionType(MS_OGR);
            }

            $oMap->save($targetDirectory . $uniqueId . ".map");

            $response->setJsonContent([
                "success" => true,
                "message" => "",
                "data" => [
                    "uniqueId" => $uniqueId,
                    "filename" => $filename,
                    "urlWfs" => $this->config->params->PRODIGE_URL_DATACARTO . "/map/wfs/temp/" . $uniqueId."/".$uniqueId,
                    "columns" => $columns,
//                    "geometryOgrinfo" => $geometryOgrinfo,
//                    "mapGeometry" => $mapGeometry,
                ]
            ]);
            return $response;
        } catch (\Exception $e) {
//            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to create map file. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    // based on original code : admincarto/carmenwsmapserv/src/Mapserver/ApiBundle/Controller/MapfileController.php    _getLayerFields
    protected function getColumnsFromOgrinfoOutput($output)
    {
        $startfield_pattern = "!^Layer SRS WKT!is";
        $field_pattern = "!^([^\s:]+)\s*:\s+([^\s:]+)!i";
        $items = array();
        $fieldListStart = false;
        $projectionCount = 0;

        foreach ($output as $outputline) {
            if (preg_match($startfield_pattern, $outputline) || $fieldListStart || $projectionCount > 0) {
                $fieldListStart = true;
                $projectionCount += substr_count($outputline, "[");
                $projectionCount -= substr_count($outputline, "]");
            }
            if ($fieldListStart && $projectionCount == 0 && preg_match($field_pattern, $outputline)) {
                preg_replace_callback($field_pattern, function($matches)use(&$items) {
                    $items[$matches[1]] = $matches[2];
                }, $outputline);
            }
        }
        return $items;
    }

    protected function getFirstGeometryFromOgrinfoOutput($output)
    {
        $geometryPrefix = "OGRFeature";
        
        for( $i=0; $i < count($output); $i++) {
            $outputline = $output[$i];
            if (preg_match("/^" . $geometryPrefix . "/", $outputline)) {
                $next = $output[$i+1];
                $currentGeometry = preg_replace("!^\s*(\w+)\W.+$!", "$1", $next);//str_replace($geometryPrefix, "", $outputline);
                return $currentGeometry;
            }
            
        }
        return null;
    }

    protected function getSrsFromOgrinfoOutput($output)
    {
        $srsPrefix = 'AUTHORITY\["EPSG","';
        //reverse since it's last info that is interesting
        $output = array_reverse ($output);
        foreach ($output as $outputline) {
            if (preg_match("/" . $srsPrefix . "/", $outputline)) {
                $currentSrs = trim(str_replace('AUTHORITY["EPSG","', "", str_replace('"]]', "", $outputline)));
                return "EPSG:".$currentSrs;
            }
        }
        return null;
    }
    

    protected function sanitizeFilename($str)
    {
        $search = array('é', 'è', 'ê', 'ë', 'ä', 'à', 'â', 'ü', 'ù', 'û', 'î', 'ï', 'ô', 'ö', 'ç'); // source is Utils.php
        $replace = array('e', 'e', 'e', 'e', 'a', 'a', 'a', 'u', 'u', 'u', 'i', 'i', 'o', 'o', 'c');

        $str = strtolower($str);
        $str = str_replace($search, $replace, $str);
        $str = mb_ereg_replace("([^_a-zA-Z0-9\-\.])", "_", $str);

        return $str;
    }

}
