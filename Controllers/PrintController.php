<?php

namespace Visualiseur\Core\Controllers;

use Visualiseur\Core\Controllers\Traits\HttpTrait;
use Visualiseur\Core\Controllers\Traits\RequestTrait;
use Visualiseur\Core\Controllers\Traits\ContextTrait;

/**
 * Class PrintController
 *
 * @package Visualiseur\Core\Controllers
 */
class PrintController extends ControllerBase
{

    use HttpTrait;
    use RequestTrait;
    use ContextTrait;

    private $varSmartyToAngularComponents = [
        'map'               =>  '<alk-build-olmap ></alk-build-olmap>',
        'scaleline'         =>  '<alk-controls-scale-line></alk-controls-scale-line>',
        'scaledrop'         =>  '<alk-map-scale-dropdown></alk-map-scale-dropdown>',
        'legend'            =>  '<alk-sidebar></alk-sidebar>',
        'keymap'            =>  '<alk-map-visualisation></alk-map-visualisation>',
        'logo'              =>  '<alk-logo></alk-logo>',
        'copyright'         =>  '<alk-copyright></alk-copyright>'
    ];

    public function exportAction()
    {
        $response = new \Phalcon\Http\Response();
        try {
            // On commence par une vérification simple des paramètres de la reqûete.
            $requestBody = $this->request->getJsonRawBody(true);

            if ( !$this->request->isPost() || !isset($requestBody) || !isset($requestBody['context']) || !isset($requestBody['model'])){
                if ($this->request->isOptions()) {
                    $response->setStatusCode(200);
                } else {
                    $response->setStatusCode(500);
                    $response->setContent("Invalid data.");
                }

                return $response;
            }


            $account = $this->request->get('account');
            $accountPath = $this->getServicePath($account);
            if(!file_exists($this->config->params->PATH_DATA.'tmpcontext/'.$account)){
                mkdir($this->config->params->PATH_DATA.'tmpcontext/'.$account, 0770);
            }

            // On vérifie maintenant que le contexte semble correct.
            if (
                isset($requestBody['context']['properties']) &&
                isset($requestBody['context']['properties']['extension']) &&
                isset($requestBody['context']['properties']['extension']['Layout']) &&
                isset($requestBody['context']['properties']['extension']['Layout']['PrintModels']) &&
                is_array($requestBody['context']['properties']['extension']['Layout']['PrintModels'])
            ) {
                $printModels = $requestBody['context']['properties']['extension']['Layout']['PrintModels'];

                if (count($printModels) > 0) {
                    $isValidContext = false;
                    foreach ($printModels as $printModel) {
                        if (isset($printModel['file'])) {
                            if ($printModel['file'] == $requestBody['model']) {
                                $isValidContext = true;
                            }
                        }
                    }
                    if ($isValidContext) {

                        // Ok le contexte semble valide, reste à vérifier la présence du modèle dans le système de fichiers.
                        if (file_exists($this->config->params->PATH_DATA.$accountPath."IHM/LayoutModels/".$requestBody['model'])) {
                            // Le fichier template existe, on prépare les variables Smarty
                            $smarty = $this->getSmarty();

                            $this->writeChart($requestBody, $smarty);

                            // Constantes
                            $smarty->assign("path_to_IHM", '');
                            $smarty->assign("print", '');
                            $smarty->assign("title_alias", '');
                            $smarty->assign("subtitle_alias", '');
                            $smarty->assign("comment_alias", '');
                            $smarty->assign("map_alias", '');
                            $smarty->assign("scaleline_alias", '');
                            $smarty->assign("legend_alias", '');
                            $smarty->assign("keymap_alias", '');
                            $smarty->assign("copyright_alias", '');
                            $smarty->assign("logo_alias", '');

                            // Variables
                            $smarty->assign("title",
                                (
                                isset($requestBody['title'])
                                    ? $requestBody['title']
                                    : (
                                isset($requestBody['context']['properties']['title'])
                                    ? $requestBody['context']['properties']['title']
                                    : ''
                                )
                                )
                            );

                            $smarty->assign("subtitle",
                                (
                                isset($requestBody['subtitle'])
                                    ? $requestBody['subtitle']
                                    : (
                                isset($requestBody['context']['properties']['subtitle'])
                                    ? $requestBody['context']['properties']['subtitle']
                                    : ''
                                )
                                )
                            );

                            $smarty->assign("comment",
                                (
                                isset($requestBody['comment'])
                                    ? $requestBody['comment']
                                    : null
                                )
                            );

                            $smarty->assign("map", $this->varSmartyToAngularComponents["map"]);

                            $smarty->assign("scaleline",
                                (
                                isset($requestBody['scalingCheckbox'])
                                    ? $this->varSmartyToAngularComponents["scaleline"]
                                    : $this->varSmartyToAngularComponents["scaleline"]
                                )
                            );

                            $smarty->assign("mentionCheckbox",
                                (
                                isset($requestBody['mentionCheckbox'])
                                    ? $requestBody['mentionCheckbox']
                                    : null
                                )
                            );

                            $smarty->assign("mentionText",
                                (
                                isset($requestBody['mentionText'])
                                    ? $requestBody['mentionText']
                                    : 'Document de travail'
                                )
                            );

                            $smarty->assign("orientation",
                                (
                                isset($requestBody['orientation'])
                                    ? $requestBody["orientation"]
                                    : 'landscape'
                                )
                            );

                            $smarty->assign("legend",
                                (
                                isset($requestBody['legendCheckbox'])
                                    ? $this->varSmartyToAngularComponents["legend"]
                                    : ''
                                )
                            );

                            $smarty->assign("keymap",
                                (
                                isset($requestBody['keyMapCheckbox'])
                                    ? $this->varSmartyToAngularComponents["keymap"]
                                    : ''
                                )
                            );

                            $smarty->assign("pageNumber",
                                (
                                isset($requestBody['pageNumber'])
                                    ? $requestBody['pageNumber']
                                    : 1
                                )
                            );

                            $smarty->assign("copyright", $this->varSmartyToAngularComponents["copyright"]);
                            $smarty->assign("logo", $this->varSmartyToAngularComponents["logo"]);

                            $template_file = null;
                            if( !file_exists($template_file = ($this->config->params->PATH_DATA.$accountPath."IHM/LayoutModels/".$requestBody['model'])) ){
                                $template_file = $this->locateResource('@ProdigeFrontCartoServicesBundle/Resources/templates/'.$requestBody['model']);
                            }

                            $templateHtml = "";
                            if ( $template_file && file_exists($template_file) ){
                                $templateHtml = $smarty->fetch($template_file);
                            } else {
                                return new Response("Print model not found.", Response::HTTP_NO_CONTENT);
                            }

                            // On doit écrire le fichier avec le template parsé pour le récuéprer plus tard.
                            $templateFileName = uniqid('tpl_', true) . '.tpl';
                            $resultTemplatePutContent = file_put_contents(
                                $this->config->params->PATH_DATA.$accountPath.'IHM/LayoutModels/' . $templateFileName,
                                $templateHtml
                            );

                            // Ensuite on doit maintenant créer le nouveau fichier contexte avec les valeurs passées en requête.
                            if (isset($requestBody['title'])) {
                                $requestBody['context']['properties']['title'] = $requestBody['title'];
                            }

                            if (isset($requestBody['subtitle'])) {
                                $requestBody['context']['properties']['subtitle'] = $requestBody['subtitle'];
                            }

                            if (
                                isset($requestBody['extent']) &&
                                isset($requestBody['context']['properties']['bbox'])
                            ) {
                                $requestBody['context']['properties']['bbox'] = $requestBody['extent'];
                            }

                            $contextFileName = uniqid('ctx_', true) . '.geojson';
                            $resultContextPutContent = file_put_contents(
                                $this->config->params->PATH_DATA.'tmpcontext/'.$account.'/' . $contextFileName,
                                json_encode($requestBody['context'])
                            );

                            $optFile = $this->writeOptionFile($requestBody,$account);

                            // On vérifie que les fichiers sont correctements crées
                            if (
                                $resultContextPutContent != 0 &&
                                $resultTemplatePutContent != 0
                            ) {
                                // Maintenant on fait appel au visualiseur lite pour l'impression PDF.
                                $outputFileName = uniqid("impr_");

                                $execOutput = null;
                                $execReturnVar = null;

                                // On récupère l'orientation de l'impression
                                $orientation = 'landscape';
                                if ( isset($requestBody['orientation']) &&
                                    (
                                        $requestBody['orientation'] === 'portrait' ||
                                        $requestBody['orientation'] === 'landscape'
                                    )
                                ) {
                                    $orientation = $requestBody['orientation'];
                                }

                                // On récupère le type d'impression
                                $impressType = 'pdf';
                                if ( isset($requestBody['type']) &&
                                    (
                                        $requestBody['type'] === 'pdf' ||
                                        $requestBody['type'] === 'jpg' ||
                                        $requestBody['type'] === 'png'
                                    )
                                ) {
                                    $impressType = $requestBody['type'];
                                }

                                $outputFileName = $outputFileName . '.' . $impressType;
                                $outputFilePathAndName = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/" . $outputFileName;

                                // On récupère le type de page
                                $pageTypeSize = 'A4';
                                if (
                                    isset($requestBody['pagesize']) &&
                                    (
                                        $requestBody['pagesize'] === 'A3' ||
                                        $requestBody['pagesize'] === 'A4'
                                    )
                                ) {
                                    $pageTypeSize = $requestBody['pagesize'];
                                }

                                // On récupère le nombre de pages
                                $pageNumber = 1;
                                if (
                                    isset($requestBody['pageNumber']) &&
                                    (
                                        $requestBody['pageNumber'] === 1 ||
                                        $requestBody['pageNumber'] === 2
                                    )
                                ) {
                                    $pageNumber = $requestBody['pageNumber'];
                                }

                                // header / footer
                                $header = isset($requestBody['options']) && isset($requestBody['options']['header']) ? $requestBody['options']['header'] : '';
                                $footer = isset($requestBody['options']) && isset($requestBody['options']['footer']) ? $requestBody['options']['footer'] : '';

                                // Exécution du script node pour la génération du fichier pdf/img
                                $strCommand = "node ".realpath(dirname(__FILE__)) ."/../Scripts/puppeteerPrinting.js ".
                                    $this->config->params->URL_PRINT." ".
                                    $account."/" .$contextFileName. ' ' .
                                    $templateFileName. ' ' .
                                    $impressType. ' ' .
                                    $outputFilePathAndName. ' '.
                                    $pageTypeSize. ' '.
                                    $orientation.' '.
                                    $optFile.' '.
                                    '"'.$header.'" '.
                                    '"'.$footer.'"';

                                exec($strCommand, $execOutput, $execReturnVar);
                                // On vérifie que le fichier existe
                                /*/
                                $response->setStatusCode(500);
                                $response->setContent("Error when writing temporary files. Not exists. --> " . $strCommand);
                                return $response;//*/
                                if (file_exists($outputFilePathAndName)) {
                                    // Pour terminer, on retourne le fichier pdf de la carte.
                                    $response->setStatusCode(200);
                                    $response->resetHeaders();
                                    $response->setHeader
                                    (
                                        'Content-Type',
                                        (
                                        $impressType === 'pdf'
                                            ? 'application/pdf'
                                            :
                                            (
                                            $impressType === 'jpg'
                                                ? 'image/jpeg'
                                                : 'image/png'
                                            )

                                        )
                                    );
                                    $response->setHeader('Content-Disposition', 'attachment; filename=test.' . $impressType);
                                    $response->setContent(file_get_contents($outputFilePathAndName));

                                    // Si pas d'erreur, on supprime le fichier fraîchement généré.
                                    unlink($outputFilePathAndName);

                                    // On supprime les fichiers temporaires.
                                    // unlink($this->config->params->PATH_DATA.$accountPath.'IHM/LayoutModels/' . $templateFileName);
                                    unlink($this->config->params->PATH_DATA.'tmpcontext/'.$account.'/' . $contextFileName);

                                } else {
                                    $response->setStatusCode(500);
                                    $response->setContent("Error when writing temporary files. Not exists. --> " . $strCommand);
                                }
                            } else {
                                $response->setStatusCode(500);
                                $response->setContent("Error when writing new data files.");
                            }
                        } else {
                            $response->setStatusCode(500);
                            $response->setContent("Print model not found.");
                        }
                    } else {
                        $response->setStatusCode(500);
                        $response->setContent("Invalid context #3.");
                    }
                } else {
                    $response->setStatusCode(500);
                    $response->setContent("Invalid context #2.");
                }
            } else {
                $response->setStatusCode(500);
                $response->setContent("Invalid context #1.");
            }

            return $response;

        } catch (\Exception $e) {
            // throw $e; //for dev
            var_dump($e);
            $response->setStatusCode(500);
            $response->setContent("Unknown error.");
        }
    }

    public function exportOfagAction()
    {
        $response = new \Phalcon\Http\Response();
        try {
            // On commence par une vérification simple des paramètres de la reqûete.
            $requestBody = $this->request->getJsonRawBody(true);
            if (
                $this->request->isPost() &&
                isset($requestBody) &&
                isset($requestBody['context']) &&
                isset($requestBody['model'])
            ) {
                $account = $this->request->get('account');
                $accountPath = $this->getServicePath($account);
                if(!file_exists($this->config->params->PATH_DATA.'tmpcontext/'.$account)){
                    mkdir($this->config->params->PATH_DATA.'tmpcontext/'.$account, 0770);
                }

                // On vérifie maintenant que le contexte semble correct.
                if (
                    isset($requestBody['context']['properties']) &&
                    isset($requestBody['context']['properties']['extension']) &&
                    isset($requestBody['context']['properties']['extension']['Layout']) &&
                    isset($requestBody['context']['properties']['extension']['Layout']['PrintModels']) &&
                    is_array($requestBody['context']['properties']['extension']['Layout']['PrintModels'])
                ) {
                    $printModels = $requestBody['context']['properties']['extension']['Layout']['PrintModels'];

                    if (count($printModels) > 0) {
                        $isValidContext = false;
                        foreach ($printModels as $printModel) {
                            if (isset($printModel['file'])) {
                                if ($printModel['file'] == $requestBody['model']) {
                                    $isValidContext = true;
                                }
                            }
                        }
                        if ($isValidContext) {

                            // Ok le contexte semble valide, reste à vérifier la présence du modèle dans le système de fichiers.
                            if (file_exists($this->config->params->PATH_DATA.$accountPath."IHM/LayoutModels/".$requestBody['model'])) {
                                // Le fichier template existe, on prépare les variables Smarty
                                $smarty = $this->getSmarty();

                                $this->writeChart($requestBody, $smarty);

                                // Constantes
                                $smarty->assign("path_to_IHM", '');
                                $smarty->assign("print", '');
                                $smarty->assign("title_alias", '');
                                $smarty->assign("subtitle_alias", '');
                                $smarty->assign("comment_alias", '');
                                $smarty->assign("identifiant_alias", '');
                                $smarty->assign("nom_alias", '');
                                $smarty->assign("prenom_alias", '');
                                $smarty->assign("refcadast_alias", '');
                                $smarty->assign("surface_alias", '');
                                $smarty->assign("sysref_alias", '');
                                $smarty->assign("coord_alias", '');
                                $smarty->assign("map_alias", '');
                                $smarty->assign("scaledrop_alias", '');
                                $smarty->assign("legend_alias", '');
                                $smarty->assign("keymap_alias", '');
                                $smarty->assign("copyright_alias", '');
                                $smarty->assign("logo_alias", '');
                                $smarty->assign("extent_alias", '');

                                // Variables
                                $smarty->assign("title",
                                    (
                                    isset($requestBody['title'])
                                        ? $requestBody['title']
                                        : (
                                    isset($requestBody['context']['properties']['title'])
                                        ? $requestBody['context']['properties']['title']
                                        : ''
                                    )
                                    )
                                );

                                $smarty->assign("subtitle",
                                    (
                                    isset($requestBody['subtitle'])
                                        ? $requestBody['subtitle']
                                        : (
                                    isset($requestBody['context']['properties']['subtitle'])
                                        ? $requestBody['context']['properties']['subtitle']
                                        : ''
                                    )
                                    )
                                );

                                $smarty->assign("comment",
                                    (
                                    isset($requestBody['comment'])
                                        ? $requestBody['comment']
                                        : null
                                    )
                                );

                                $smarty->assign("identifiant",
                                    (
                                    isset($requestBody['identifiant'])
                                        ? $requestBody['identifiant']
                                        : null
                                    )
                                );

                                $smarty->assign("nom",
                                    (
                                    isset($requestBody['nom'])
                                        ? $requestBody['nom']
                                        : null
                                    )
                                );

                                $smarty->assign("prenom",
                                    (
                                    isset($requestBody['prenom'])
                                        ? $requestBody['prenom']
                                        : null
                                    )
                                );

                                $smarty->assign("refcadast",
                                    (
                                    isset($requestBody['refcadast'])
                                        ? $requestBody['refcadast']
                                        : null
                                    )
                                );

                                $smarty->assign("surface",
                                    (
                                    isset($requestBody['surface'])
                                        ? $requestBody['surface']
                                        : null
                                    )
                                );

                                $smarty->assign("sysref",
                                    (
                                    isset($requestBody['sysref'])
                                        ? $requestBody['sysref']
                                        : (
                                    isset($requestBody['context']['crs']['properties']['name'])
                                        ? explode(':', $requestBody['context']['crs']['properties']['name'])[count(explode(':', $requestBody['context']['crs']['properties']['name'])) - 2]
                                        : ''
                                    )
                                    )
                                );

                                $smarty->assign("coord",
                                    (
                                    isset($requestBody['coord'])
                                        ? $requestBody['coord']
                                        : null
                                    )
                                );

                                $smarty->assign("map", $this->varSmartyToAngularComponents["map"]);

                                $smarty->assign("scaledrop",
                                    (
                                    isset($requestBody['scalingDropdown'])
                                        ? $this->varSmartyToAngularComponents["scaledrop"]
                                        : $this->varSmartyToAngularComponents["scaledrop"]
                                    )
                                );

                                $smarty->assign("mentionCheckbox",
                                    (
                                    isset($requestBody['mentionCheckbox'])
                                        ? $requestBody['mentionCheckbox']
                                        : null
                                    )
                                );

                                $smarty->assign("mentionText",
                                    (
                                    isset($requestBody['mentionText'])
                                        ? $requestBody['mentionText']
                                        : 'Document de travail'
                                    )
                                );

                                $smarty->assign("orientation",
                                    (
                                    isset($requestBody['orientation'])
                                        ? $requestBody["orientation"]
                                        : 'landscape'
                                    )
                                );

                                $smarty->assign("legend",
                                    (
                                    isset($requestBody['legendCheckbox'])
                                        ? $this->varSmartyToAngularComponents["legend"]
                                        : ''
                                    )
                                );

                                $smarty->assign("keymap",
                                    (
                                    isset($requestBody['keyMapCheckbox'])
                                        ? $this->varSmartyToAngularComponents["keymap"]
                                        : ''
                                    )
                                );

                                $smarty->assign("pageNumber",
                                    (
                                    isset($requestBody['pageNumber'])
                                        ? $requestBody['pageNumber']
                                        : 1
                                    )
                                );

                                $smarty->assign("copyright", $this->varSmartyToAngularComponents["copyright"]);
                                $smarty->assign("logo", $this->varSmartyToAngularComponents["logo"]);

                                $template_file = null;
                                if( !file_exists($template_file = ($this->config->params->PATH_DATA.$accountPath."IHM/LayoutModels/".$requestBody['model'])) ){
                                    $template_file = $this->locateResource('@ProdigeFrontCartoServicesBundle/Resources/templates/'.$requestBody['model']);
                                }

                                $templateHtml = "";
                                if ( $template_file && file_exists($template_file) ){
                                    $templateHtml = $smarty->fetch($template_file);
                                } else {
                                    return new Response("Print model not found.", Response::HTTP_NO_CONTENT);
                                }

                                // On doit écrire le fichier avec le template parsé pour le récuéprer plus tard.
                                $templateFileName = uniqid('tpl_', true) . '.tpl';
                                $resultTemplatePutContent = file_put_contents(
                                    $this->config->params->PATH_DATA.$accountPath.'IHM/LayoutModels/' . $templateFileName,
                                    $templateHtml
                                );

                                // Ensuite on doit maintenant créer le nouveau fichier contexte avec les valeurs passées en requête.
                                if (isset($requestBody['title'])) {
                                    $requestBody['context']['properties']['title'] = $requestBody['title'];
                                }

                                if (isset($requestBody['subtitle'])) {
                                    $requestBody['context']['properties']['subtitle'] = $requestBody['subtitle'];
                                }

                                // Calcul du centre de l'extent
                                $extent = $requestBody['context']['properties']['bbox'];

                                $minx = 99999999;
                                $maxx = -99999999;
                                $miny = 99999999;
                                $maxy = -99999999;
                                foreach($requestBody['coord'] as $ind => $coord){
                                    $minx = min($coord[0], $minx);
                                    $maxx = max($coord[0], $maxx);
                                    $miny = min($coord[1], $miny);
                                    $maxy = max($coord[1], $maxy);
                                }
                                /*$x = $extent[0] + ($extent[2] - $extent[0])/2;
                                $y = $extent[1] + ($extent[3] - $extent[1])/2;*/
                                $x = ($minx + $maxx)/2;
                                // hack... ajout d'un décalage, pour centrer le polygone, décalage vers le bas sinon
                                $y = ($miny + $maxy)/2 - 450;
                                $centerOfExtent = [$x, $y];

                                if (
                                    isset($requestBody['extent']) &&
                                    isset($requestBody['context']['properties']['bbox'])
                                ) {
                                    $requestBody['context']['properties']['bbox'] = $requestBody['extent'];
                                } else {
                                    $requestBody['context']['properties']['bbox'] = $this->computeExtent([600, 600], $centerOfExtent, 36100);
                                }

                                $contextFileName = uniqid('ctx_', true) . '.geojson';
                                $resultContextPutContent = file_put_contents(
                                    $this->config->params->PATH_DATA.'tmpcontext/'.$account.'/' . $contextFileName,
                                    json_encode($requestBody['context'])
                                );

                                $optFile = $this->writeOptionFile($requestBody,$account);

                                // Modification de l'extent et enregistrement d'un 2e contexte
                                if (
                                    isset($requestBody['extent']) &&
                                    isset($requestBody['context']['properties']['bbox'])
                                ) {
                                    $requestBody['context']['properties']['bbox'] = $requestBody['extent'];
                                } else {
                                    $requestBody['context']['properties']['bbox'] = $this->computeExtent([600, 600], $centerOfExtent, 18100);
                                }

                                $contextFileNameSecond = uniqid('ctx_', true) . '.geojson';
                                $resultContextPutContentSecond = file_put_contents(
                                    $this->config->params->PATH_DATA.'tmpcontext/'.$account.'/' . $contextFileNameSecond,
                                    json_encode($requestBody['context'])
                                );

                                // On vérifie que les fichiers sont correctements crées
                                if (
                                    $resultContextPutContent != 0 &&
                                    $resultContextPutContentSecond != 0 &&
                                    $resultTemplatePutContent != 0
                                ) {
                                    // Maintenant on fait appel au visualiseur lite pour l'impression PDF.
                                    $outputFileName = uniqid("impr_");

                                    $execOutput = null;
                                    $execReturnVar = null;

                                    // On récupère l'orientation de l'impression
                                    $orientation = 'landscape';
                                    if (
                                        isset($requestBody['orientation']) &&
                                        (
                                            $requestBody['orientation'] === 'portrait' ||
                                            $requestBody['orientation'] === 'landscape'
                                        )
                                    ) {
                                        $orientation = $requestBody['orientation'];
                                    }

                                    // On récupère le type d'impression
                                    $impressType = 'pdf';
                                    if (
                                        isset($requestBody['type']) &&
                                        (
                                            $requestBody['type'] === 'pdf' ||
                                            $requestBody['type'] === 'jpg' ||
                                            $requestBody['type'] === 'png'
                                        )
                                    ) {
                                        $impressType = $requestBody['type'];
                                    }

                                    $outputFilePath = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/";

                                    $outputFileName = $outputFileName . '.' . $impressType;
                                    $outputFilePathAndName = $outputFilePath . $outputFileName;

                                    $outputFileNameSecond = $outputFileName . '_second' . '.' . $impressType;
                                    $outputFilePathAndNameSecond = $outputFilePath . $outputFileNameSecond;

                                    // On récupère le type de page
                                    $pageTypeSize = 'A4';
                                    if (
                                        isset($requestBody['pagesize']) &&
                                        (
                                            $requestBody['pagesize'] === 'A3' ||
                                            $requestBody['pagesize'] === 'A4'
                                        )
                                    ) {
                                        $pageTypeSize = $requestBody['pagesize'];
                                    }

                                    // On récupère le nombre de pages
                                    $pageNumber = 1;
                                    if (
                                        isset($requestBody['pageNumber']) &&
                                        (
                                            $requestBody['pageNumber'] === 1 ||
                                            $requestBody['pageNumber'] === 2
                                        )
                                    ) {
                                        $pageNumber = $requestBody['pageNumber'];
                                    }

                                    // Exécution du script node pour la génération du fichier pdf/img
                                    $strCommand = "node ".realpath(dirname(__FILE__)) ."/../Scripts/puppeteerPrinting.js ".
                                        $this->config->params->URL_PRINT." ".
                                        $account."/" .$contextFileName. ' ' .
                                        $templateFileName. ' ' .
                                        $impressType. ' ' .
                                        $outputFilePathAndName. ' '.
                                        $pageTypeSize. ' '.
                                        $orientation.' '.
                                        $optFile;

                                    exec($strCommand, $execOutput, $execReturnVar);

                                    // Nouvelle exécution du script node pour le 2e pdf
                                    $strCommandSecond = "node ".realpath(dirname(__FILE__)) ."/../Scripts/puppeteerPrinting.js ".
                                        $this->config->params->URL_PRINT." ".
                                        $account."/" .$contextFileNameSecond. ' ' .
                                        $templateFileName. ' ' .
                                        $impressType. ' ' .
                                        $outputFilePathAndNameSecond. ' '.
                                        $pageTypeSize. ' '.
                                        $orientation.' '.
                                        $optFile;

                                    exec($strCommandSecond, $execOutput, $execReturnVar);

                                    // On vérifie que le fichier existe

                                    if (file_exists($outputFilePathAndName) && file_exists($outputFilePathAndNameSecond)) {
                                        // Pour terminer, on retourne le fichier pdf de la carte.
                                        $response->setStatusCode(200);
                                        $response->resetHeaders();
                                        $response->setHeader
                                        (
                                            'Content-Type',
                                            (
                                            $impressType === 'pdf'
                                                ? 'application/pdf'
                                                :
                                                (
                                                $impressType === 'jpg'
                                                    ? 'image/jpeg'
                                                    : 'image/png'
                                                )

                                            )
                                        );
                                        $response->setHeader('Content-Disposition', 'attachment; filename=test.' . $impressType);

                                        $pdfwrite = $this->config->params->PATH_DATA . $this->config->params->MAPIMAGE_DIR. "/" . $outputFileName . '_combined' . '.' . $impressType;
                                        exec('gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$pdfwrite.' -dBATCH '.$outputFilePathAndName.'  '.$outputFilePathAndNameSecond.' ', $finalPdf) ;

                                        $response->setContent(file_get_contents($pdfwrite));

                                        // Si pas d'erreur, on supprime le fichier fraîchement généré.
                                        unlink($pdfwrite);

                                        // On supprime les fichiers temporaires.
                                        unlink($this->config->params->PATH_DATA.$accountPath.'IHM/LayoutModels/' . $templateFileName);
                                        unlink($this->config->params->PATH_DATA.'tmpcontext/'.$account.'/' . $contextFileName);

                                    } else {
                                        $response->setStatusCode(500);
                                        $response->setContent("Error when writing temporary files. Not exists. --> " . $strCommand);
                                    }
                                } else {
                                    $response->setStatusCode(500);
                                    $response->setContent("Error when writing new data files.");
                                }
                            } else {
                                $response->setStatusCode(500);
                                $response->setContent("Print model not found.");
                            }
                        } else {
                            $response->setStatusCode(500);
                            $response->setContent("Invalid context #3.");
                        }
                    } else {
                        $response->setStatusCode(500);
                        $response->setContent("Invalid context #2.");
                    }
                } else {
                    $response->setStatusCode(500);
                    $response->setContent("Invalid context #1.");
                }
            } else {
                if ($this->request->isOptions()) {
                    $response->setStatusCode(200);
                } else {
                    $response->setStatusCode(500);
                    $response->setContent("Invalid data.");
                }
            }
            return $response;

        } catch (\Exception $e) {
            // throw $e; //for dev
            var_dump($e);
            $response->setStatusCode(500);
            $response->setContent("Unknown error.");
        }
    }

    /**
     * Récupère le template Smarty parsé pour l'impression PDF.
     * Si non trouvé, renvoie un 404.
     *
     * URL: /core/print/getTemplate/{templateFileName}
     *
     * @param templateFileName Nom du template HTML à retourner. Doit commencer par 'tpl_'  et terminer par '.tpl'
     * @return String Contenu HTML template PDF.
     */
    public function getTemplateAction($templateFileName) {

        $account = $this->request->get('account');
        $accountPath = $this->getServicePath($account);

        $response = new \Phalcon\Http\Response();
        try {
            // On commence par une vérification simple des paramètres de la reqûete.
            if (
                isset($templateFileName) &&
                is_string($templateFileName) &&
                pathinfo($templateFileName)['extension'] = 'tpl'
            ) {
                // Maintenant, on regarde si le fichier existe dans le système de fichier
                if (file_exists(
                    $this->config->params->PATH_DATA.$accountPath.'IHM/LayoutModels/' . $templateFileName
                )) {
                    $response->setStatusCode(200);
                    $response->setContent(
                        json_encode([
                            "template" => file_get_contents($this->config->params->PATH_DATA.$accountPath.'IHM/LayoutModels/' . $templateFileName)
                        ])
                    );
                } else {
                    $response->setStatusCode(404);
                    $response->setContent("Template not found.");
                }
            } else {
                $response->setStatusCode(500);
                $response->setContent("Invalid parameters.");
            }
            return $response;
        } catch (\Exception $e) {
            $response->setStatusCode(500);
            $response->setContent("Unknown error.");
        }
    }

    /**
     * Instancie et configure un objet Smarty
     * @return \Smarty
     */
    private function getSmarty()
    {
        //assign Data
        $smarty = new \Smarty();
        $smarty->compile_check = true;

        return $smarty;
    }

    /**
     * @param array $imageDisplay  [0] width  [1] height
     * @param array $coordinates   [0] x      [1] y
     * @param double $scale
     */
    function computeExtent ($imageDisplay = [600, 600], $coordinates, $scale = 9500000) {
        /* dividing image width by DPI to get it in Inch */
        $imgWidth = $imageDisplay[0] / 96;
        $imgHeight = $imageDisplay[1] / 96;

        /* converting Inch to meter (assume the map is in meter) */
        $widthMapUnit = $imgWidth * 0.0254;
        $heightMapUnit = $imgHeight * 0.0254;

        /* calculating half of map’s height & width at the specific scale */
        $dX = ($widthMapUnit * $scale) / 2;
        $dY = ($heightMapUnit * $scale) / 2;

        /* using the center point and width & height created before, it is computing map’s extent */
        $minX = $coordinates[0] - $dX;
        $maxX = $coordinates[0] + $dX;
        $minY = $coordinates[1] - $dY;
        $maxY = $coordinates[1] + $dY;

        return [$minX, $minY, $maxX, $maxY];
    }

    /**
     * @param array $requestBody
     * @param $smarty
     * @return bool
     */
    private function writeChart(array  $requestBody, $smarty):bool{
        if(!isset($requestBody['options']) || !isset($requestBody['options']['graph']) || !isset($requestBody['options']['fields'])){
            return false;
        }

        foreach ($requestBody['options']['graph'] as $key => $chartOption){
            if(isset($chartOption['templateId'])){
                $htmlChartStr = '<alk-chart-builder template-id="'.$chartOption['templateId'].'" ></<alk-chart-builder>';
                $smarty->assign($chartOption['templateId'], $htmlChartStr);
            }
        }

        return true;
    }

    /**
     * @param array $requestBody
     * @param string $account
     * @return string|bool
     */
    private function writeOptionFile(array $requestBody, string  $account) {
        if(!isset($requestBody['options']) || !$requestBody['options']){
            return false;
        }

        $contextFileNameSecond = uniqid('opt_', true) . '.json';
        $resultContextPutContentSecond = file_put_contents(
            $this->config->params->PATH_DATA.'tmpcontext/'.$account.'/' . $contextFileNameSecond,
            json_encode($requestBody['options'])
        );

        return $resultContextPutContentSecond ? $contextFileNameSecond : false;
    }

    /**
     * Récupère le fichier d'option json pour l'impression pdf
     * Si non trouvé, renvoie un 404.
     *
     * URL: /core/print/getOptions/{optionFileName}
     *
     * @param templateFileName Nom du template HTML à retourner. Doit commencer par 'tpl_'  et terminer par '.tpl'
     * @return String Contenu HTML template PDF.
     */
    public function getOptionsAction($optionFilename) {
        $account = $this->request->get('account');
        $accountPath = $this->getServicePath($account);

        $response = new \Phalcon\Http\Response();
        if (!isset($optionFilename) ||  !is_string($optionFilename) ||  pathinfo($optionFilename)['extension'] != 'json'){
            $response->setStatusCode(500);
            $response->setContent("getOptionsAction Invalid parameters. : ".$optionFilename);

            return $response;
        }

        if (!file_exists($this->config->params->PATH_DATA.'tmpcontext/'.$account.'/' . $optionFilename)){
            $response->setStatusCode(404);
            $response->setContent("Template not found.");

            return $response;
        }

        try {
            $response->setStatusCode(200);
            $response->setContent( file_get_contents($this->config->params->PATH_DATA.'tmpcontext/'.$account.'/'. $optionFilename));

        } catch (\Exception $e) {
            $response->setStatusCode(500);
            $response->setContent($e->getMessage());
        }

        return $response;
    }
}
