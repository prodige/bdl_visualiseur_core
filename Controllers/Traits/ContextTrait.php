<?php

namespace Visualiseur\Core\Controllers\Traits;

/**
 * ContextTrait
 */
Trait ContextTrait
{

    /**
     * get account path according to account id, default is cartes
     */
    public function getServicePath($account){

        $serviceFile = $this->config->params->PATH_DATA . "SYSTEM/services.xml";
        
        $doc = new \DOMDocument();
        if (@$doc->load($serviceFile)){
            
            $xpath = new \DOMXpath($doc);
            $result = $xpath->query('/services/service[@idx="'.$account.'"]');
            
            if (!is_null($result) && ($result->length!=0)) {
                foreach ($result as $element) {
                  return $element->getAttributeNode("path")->nodeValue;
                }
            }else{
                return "cartes/";    
            }
        }else{
            return "cartes/";
        }

    }

    /**
     * Context action
     */
    public function getContext()
    {
        $account = $this->request->getQuery('account');
        $accountPath = $this->getServicePath($account);
        $contextPath = $this->request->getQuery('contextPath');
        //for jsonp request
        $callback = $this->request->getQuery('callback', null);
        
        if (is_null($account) || is_null($contextPath)) {
            header("HTTP/1.0 400 Bad Request");
            return null;
        }
        $checkRights = true;
        
        if ($this->endsWith($contextPath, ".tmp")) {
            $contextFolder = $this->config->params->PATH_DATA . "/tmpcontext/" . $account . '/';
            $contextFilename = str_replace('.tmp', '.geojson', $contextPath);
            
            $checkRights = false;
        }
        elseif ($this->endsWith($contextPath, ".geojson")) {
            $contextFolder = $this->config->params->PATH_DATA . "/savedcontext/" . $account . '/';
            $contextFilename = $contextPath;
        } else {
            $contextFolder = $this->config->params->PATH_DATA . "/owscontext/" . $account . '/';
            $contextFilename = str_replace('.map', '.geojson', $contextPath);
            //temp context not checked
            if($this->beginsWith($contextPath, "/layers/TMP_") || $this->beginsWith($contextPath, "/carteperso/") || $this->beginsWith($contextPath, "/MODEL_")){
                $checkRights = false;
             }

        }

        if (file_exists($contextFolder . $contextFilename)) {
            
            $fileContent = file_get_contents($contextFolder . $contextFilename);
            $fileContentAsJson = json_decode($fileContent, true);

            //check rights with original mapname 
            if(isset( $fileContentAsJson["originalId"])){
                $mapName = $fileContentAsJson["originalId"];    
            }else{
                $mapName = $fileContentAsJson["id"];
            }
            
            if ($checkRights && !$this->checkRights($mapName, $account)) {
               
                $message = new \stdClass();
                $message->success = false;

                if($this->isUserInternet()){
                    $message->code = 401;
                }else{
                    $message->code = 403;
                }
                
                $this->sendJsonResponse($message, $callback);
               
                exit();
            }
          

            $group = $this->request->getQuery('group');
            $layer = $this->request->getQuery('layer');
            $object = $this->request->getQuery('object');
            if (!is_null($group)) {
                $groups = explode(";", $group);
            } else {
                $groups = array();
            }
            if (!is_null($layer)) {
                $layers = explode(";", $layer);
            } else {
                $layers = array();
            }

            $object = $this->request->getQuery('object');
            
            //try WFS Request to find object and get associated extent
            // exemple : object=layer6;identifian;2015-001457
            if($object!=""){
                $queryExtent = $this->getObjectExtent($object, $fileContentAsJson, $accountPath, $account);
                
             
                if($queryExtent && is_array($queryExtent)){
                    //ajout d'une tolérance pour les objets ponctuels
                    //TODO :gérer le 4326
                    if($queryExtent[0]["minx"]==$queryExtent[0]["maxx"]){
                        $queryExtent[0]["minx"] = $queryExtent[0]["minx"]-100;
                        $queryExtent[0]["maxx"] = $queryExtent[0]["maxx"]+100;
                    }
                    if($queryExtent[0]["miny"]==$queryExtent[0]["maxy"]){
                        $queryExtent[0]["miny"] = $queryExtent[0]["miny"]-100;
                        $queryExtent[0]["maxy"] = $queryExtent[0]["maxy"]+100;
                    }
                    $fileContentAsJson["properties"]["bbox"][0] = (float) $queryExtent[0]["minx"];
                    $fileContentAsJson["properties"]["bbox"][1] = (float) $queryExtent[0]["miny"];
                    $fileContentAsJson["properties"]["bbox"][2] = (float) $queryExtent[0]["maxx"];
                    $fileContentAsJson["properties"]["bbox"][3] = (float) $queryExtent[0]["maxy"];
                    $layers = array($queryExtent[1]);

                    $geoJson = $queryExtent[2];
                    if($geoJson){
                        $fileContentAsJson["properties"]["extension"]["queryFeature"] = json_decode($geoJson);
                    }
                }
                
            }

            if (count($layers) > 0 || count($groups) > 0) {
                if (array_key_exists('properties', $fileContentAsJson) && array_key_exists('extension', $fileContentAsJson["properties"]) && array_key_exists('layers', $fileContentAsJson["properties"]["extension"])) {
                    $result = $this->searchInLayers($fileContentAsJson["properties"]["extension"]["layers"], $groups, $layers, false);
                    // applying modifications
                    $fileContentAsJson["properties"]["extension"]["layers"] = $result;
                }
            }
            if (count($layers) > 0) {
                $this->potentialApiMisuseDetected("Failed to find the requested layer : " . $layers[0]);
            }
            if (count($groups) > 0) {
                $this->potentialApiMisuseDetected("Failed to find the requested group : " . $groups[0]);
            }

            $extent = $this->request->getQuery('extent');
            if (!is_null($extent)) {
                $extents = explode(",", $extent);
                if (count($extents) != 4) {
                    // should be commented in production
//                    throw new \Exception("parameter extent is invalid (wrong length).");
                    $this->potentialApiMisuseDetected("parameter extent is invalid (wrong length).");
                } else if (!is_numeric($extents[0]) || !is_numeric($extents[1]) || !is_numeric($extents[2]) || !is_numeric($extents[3])) {
                    $this->potentialApiMisuseDetected("parameter extent is invalid (not is_numeric).");
                } else if (!array_key_exists('properties', $fileContentAsJson) || !array_key_exists('bbox', $fileContentAsJson["properties"])) {
                    $this->potentialApiMisuseDetected("json file content is invalid (json.properties.bbox is missing).");
                } else {
                    // provided parameter is valid
                    $fileContentAsJson["properties"]["bbox"][0] = (float) $extents[0];
                    $fileContentAsJson["properties"]["bbox"][1] = (float) $extents[1];
                    $fileContentAsJson["properties"]["bbox"][2] = (float) $extents[2];
                    $fileContentAsJson["properties"]["bbox"][3] = (float) $extents[3];
//                    throw new \Exception("log__" . var_export($fileContentAsJson["properties"]["bbox"], true));
                }
            }
            
            

            if($checkRights){
                if(method_exists ($this , 'updateLocatorParameters')){
                    $fileContentAsJson = $this->updateLocatorParameters($fileContentAsJson);
                }
                
                //surcharge tools from rights
                if(method_exists ($this , 'updateTools')){
                    $fileContentAsJson = $this->updateTools($fileContentAsJson);
                } 
            }

            if(method_exists ($this , 'addLog') && $this->endsWith($contextPath, ".map") && $checkRights){
                $this->addLog($fileContentAsJson);
            }

            $this->sendJsonResponse($fileContentAsJson, $callback);
            exit();
            
        }

        header("HTTP/1.0 404 Not Found");
        exit();
    }

    /**
     * run WFS Query and return bbox if found
     * @param $object : query params
     * @param $contextJson : json Context
     * @return bbox or false
     */
    protected function getObjectExtent($object, $contextJson, $accountPath, $account){
        

        //service WFS to datacarto
        $map = $this->config->params->PATH_DATA .$accountPath."Publication/".$contextJson["id"].".map";
      
        if(file_exists($map)){
            //do it better
            if(!isset($this->config->params->ACCOUNT_ID_IN_WXS) || $this->config->params->ACCOUNT_ID_IN_WXS===false){
                $url = $this->config->params->URL_DATACARTO ."/map/"."/".$contextJson["id"];
            }else{
                $url = $this->config->params->URL_DATACARTO ."/map/". $account."/".$contextJson["id"];
            }
            
            $properties = explode(";", $object);

            if(count($properties)<3){
                throw new \Exception("invalid number of object arguments");
            }
            $layerTitle = $properties[0];
            $layerName = $this->getLayerName($layerTitle, $contextJson["properties"]["extension"]["layers"]);
            
            
            $key = $properties[1];
            $value = $properties[2];

            if(count($properties) == 3){
                $data = '<?xml version="1.0" encoding="UTF-8"?> <GetFeature service="WFS" version="2.0.0" count="100" outputformat="geojson" '.
                    'xmlns="http://www.opengis.net/wfs/2.0" xmlns:cw="http://www.someserver.com/cw" xmlns:fes="http://www.opengis.net/ogc/1.1" '.
                    'xmlns:gml="http://www.opengis.net/gml/3.2" > <Query typeNames="'.$layerName.'" propertyName="'.$key.'"  styles="default"><Filter>'.
                    '<PropertyIsEqualTo><PropertyName>'.$key.'</PropertyName><Literal>'.$value.'</Literal></PropertyIsEqualTo></Filter></Query></GetFeature>';
            } else {
                $data = '<?xml version="1.0" encoding="UTF-8"?> <GetFeature service="WFS" version="2.0.0" count="100" outputformat="geojson" '.
                'xmlns="http://www.opengis.net/wfs/2.0" xmlns:cw="http://www.someserver.com/cw" xmlns:fes="http://www.opengis.net/ogc/1.1" '.
                'xmlns:gml="http://www.opengis.net/gml/3.2" > <Query typeNames="'.$layerName.'" propertyName="'.$key.'"  styles="default"><Filter><Or>';

                $tabWdpaid = $properties;
                unset($tabWdpaid[0]);
                unset($tabWdpaid[1]);

                foreach($tabWdpaid as $wdpaid){
                    $data .= '<PropertyIsEqualTo><PropertyName>'.$key.'</PropertyName><Literal>'.$wdpaid.'</Literal></PropertyIsEqualTo>';
                }
                $data .= '</Or></Filter></Query></GetFeature>';
            }

            $ch = curl_init($url);
            $optEnabled = curl_setopt($ch, CURLOPT_URL, $url)
            && curl_setopt($ch, CURLOPT_POST, true)
            && curl_setopt($ch, CURLOPT_RETURNTRANSFER, true)
            && curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
            )
            && curl_setopt($ch, CURLOPT_HEADER, false)      

            && curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false)      
            && curl_setopt($ch, CURLOPT_POSTFIELDS, $data) // send request body
           ;

           if( !$optEnabled ) {
               throw new \Exception(sprintf("Unable to parameter curl connexion : [%s] %s", curl_errno($ch), curl_error($ch)), 500);
           }
           
           $geoJson = curl_exec($ch);
           
           
           //transform featureCollection to extent
           try{
               $featureCollection = \geoPHP::load($geoJson, 'json');
            
               if($featureCollection){
                   $bbox = $featureCollection->getBBox();
                   return array($bbox, $layerName, $geoJson);
               }
            } catch (\Exception $e) {
                //throw $e; //for dev
                error_log("error with WFS request :". $geoJson);
                return false;
            }

           return false;

        }

        
      
    }

    public function saveContext()
    {
        $this->view->disable();
        $response = new \Phalcon\Http\Response();
        try {
            if (!$this->request->isPost()) {
                $response->setJsonContent([
                    "success" => false,
                    "message" => "Post Request is mandatory.",
                ]);
                return $response;
            }

            $json = json_decode($this->request->getRawBody(), true);

            if ($json == null) {
                throw new \Exception("Failed to parse JSON. Content length is " . strlen($this->request->getRawBody()));
            }

            if (!isset($json["context"])) {
                throw new \Exception("parameter context is missing.");
            }
            if (!isset($json["lifetime"])) {
                throw new \Exception("parameter lifetime is missing.");
            }

            if (!is_int($json["lifetime"])) {
                throw new \Exception("parameter lifetime is not an integer.");
            }

            $account = $this->request->get("account");
            if($account==""){
                $account="1";
            }
           
            $context = $json["context"];
            $lifetime = $json["lifetime"];
            $name = uniqid() . ".geojson";

            $filePath = $this->config->params->PATH_DATA . "savedcontext/" . $account . "/" . $name; // /prodige/savedcontext/1
            
            if (!file_exists($this->config->params->PATH_DATA . "savedcontext/" . $account)) {
                mkdir($this->config->params->PATH_DATA . "savedcontext/" . $account, 0770);
            }

            $fileUrl = $this->config->params->URL_FRONTCARTO . "/" . $account . "/" . $name;
            $messageForUser = "Votre contexte de carte restera accessible pendant " . $lifetime . " jours.";
            $jsonContext = json_encode($context);

            file_put_contents($filePath, $jsonContext);

            //$response->setJsonContent();
            $this->sendJsonResponse([
                "success" => true,
                "message" => "",
                "data" => [
                    "url" => $fileUrl,
                    "msg" => $messageForUser,
                ]
            ]) ;
            exit();
            
        } catch (\Exception $e) {
            throw $e; //for dev
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to process file(s). " . $e->getMessage(),
            ]);
            return $response;
        }
    }

     /**
     * Check rights on map
     */
    protected function checkRights($contextFilename)
    {
        if(!method_exists ($this , "isUserHasRightOnMap") ){
            return true;
        }
        if($this->isUserHasRightOnMap($contextFilename)) {
            return true;
        }
        return false;
    }

    /**
     * check if the end of the string
     */
    protected function endsWith($string, $endString)
    {
        $len = strlen($endString);
        if ($len == 0) {
            return true;
        }
        return (substr($string, -$len) === $endString);
    }
    
    /**
     * check the begining of the string
     */
    protected function beginsWith($string, $beginString)
    {
       $len = strlen($beginString);
        if ($len == 0) {
            return false;
        }
        return (substr($string, 0, $len) === $beginString);
    }

    /**
     * May throw an exception (to notify the developer) or silently drop the warning
     * In production, aim is to return the original json without doing any modification (hard to debug).
     *
     * @param type $message
     * @throws \Exception
     */
    protected function potentialApiMisuseDetected($message)
    {
//        $debug = true; // for debug/test purpose
        $debug = false; // prod
        if ($debug) {
            throw new \Exception($message);
        }
    }

    protected function searchInLayers($rootLayer, &$groups, &$layers, $parentGroupVisible)
    {
        
        if (array_key_exists('class', $rootLayer) && strcmp($rootLayer["class"], "LayerGroup") == 0) {
            $currentGroupVisible = $parentGroupVisible;
            if (array_key_exists('extension', $rootLayer) && array_key_exists('name', $rootLayer["extension"]) && in_array($rootLayer["extension"]["name"], $groups)) {
                $currentGroupVisible = true;
                $groups = array_diff($groups, array($rootLayer["extension"]["name"]));
            }
            if (array_key_exists('layers', $rootLayer) && is_array($rootLayer["layers"])) {
                foreach ($rootLayer["layers"] as $key => $childLayer) {
                    $result = $this->searchInLayers($childLayer, $groups, $layers, $currentGroupVisible);
                    // applying modifications
                    $rootLayer["layers"][$key] = $result;
                }
            }
        }
        if (array_key_exists('class', $rootLayer) && strcmp($rootLayer["class"], "LayerGroup") != 0 && array_key_exists('extension', $rootLayer) && array_key_exists('mapName', $rootLayer["extension"])) {
            
            //test on layerName or name (layer name in mapfile or layer title)
            if ($parentGroupVisible || in_array($rootLayer["extension"]["name"], $layers)) {
                if (array_key_exists('options', $rootLayer)) { // && array_key_exists('visible', $rootLayer["options"])
                    $rootLayer["options"]["visible"] = true;
                }
                $layers = array_diff($layers, array($rootLayer["extension"]["name"]));
            }
            if ($parentGroupVisible || in_array($rootLayer["extension"]["layerName"], $layers)) {
                if (array_key_exists('options', $rootLayer)) { // && array_key_exists('visible', $rootLayer["options"])
                    $rootLayer["options"]["visible"] = true;
                }
                $layers = array_diff($layers, array($rootLayer["extension"]["layerName"]));
            }
        }
        return $rootLayer;
    }

    /**
     * Search layerName according to title or identifier
     * @param layerTitle : title or identifier
     * @param contextLayers : context obj
     * return layerName in mapfile 
     */         
    protected function getLayerName($layerTitle, $contextLayers){
        if (array_key_exists('class', $contextLayers) && strcmp($contextLayers["class"], "LayerGroup") !== 0) {
           //test with title
            if (array_key_exists('extension', $contextLayers) && 
                array_key_exists('name', $contextLayers["extension"]) 
                && strcmp($contextLayers["extension"]["name"],$layerTitle)===0) {
                return $contextLayers["extension"]["layerName"];
            }
            //test with identifier
            if (array_key_exists('extension', $contextLayers) && 
                array_key_exists('name', $contextLayers["extension"]) 
                && strcmp($contextLayers["extension"]["layerIdentifier"],$layerTitle)===0) {
                return $contextLayers["extension"]["layerName"];
            }
            //test with mapfile layername
            if (array_key_exists('extension', $contextLayers) && 
                array_key_exists('name', $contextLayers["extension"]) 
                && strcmp($contextLayers["extension"]["layerName"],$layerTitle)===0) {
                return $contextLayers["extension"]["layerName"];
            }
        }else{
            //pour les groupes, parcours du tableau de layers
            foreach ($contextLayers["layers"] as $key => $contextLayer){
                $layerName = $this->getLayerName($layerTitle, $contextLayer);
                if($layerName!=null){
                    return $layerName;
                }
            }
        }
        return null;
    }
    
}
