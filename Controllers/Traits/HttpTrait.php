<?php

namespace Visualiseur\Core\Controllers\Traits;

/**
 * HttpTrait
 */
trait HttpTrait {
  
    protected static $httpStatusCodes = array(
        100 => "Continue", 101 => "Switching Protocols", 102 => "Processing", 
        200 => "OK", 201 => "Created", 202 => "Accepted", 203 => "Non-Authoritative Information", 204 => "No Content", 205 => "Reset Content", 206 => "Partial Content", 207 => "Multi-Status", 
        300 => "Multiple Choices", 301 => "Moved Permanently", 302 => "Found", 303 => "See Other", 304 => "Not Modified", 305 => "Use Proxy", 306 => "(Unused)", 307 => "Temporary Redirect", 308 => "Permanent Redirect", 
        400 => "Bad Request", 401 => "Unauthorized", 402 => "Payment Required", 403 => "Forbidden", 404 => "Not Found", 405 => "Method Not Allowed", 406 => "Not Acceptable", 407 => "Proxy Authentication Required", 408 => "Request Timeout", 409 => "Conflict", 410 => "Gone", 411 => "Length Required", 412 => "Precondition Failed", 413 => "Request Entity Too Large", 414 => "Request-URI Too Long", 415 => "Unsupported Media Type", 416 => "Requested Range Not Satisfiable", 417 => "Expectation Failed", 418 => "I'm a teapot", 419 => "Authentication Timeout", 420 => "Enhance Your Calm", 422 => "Unprocessable Entity", 423 => "Locked", 424 => "Failed Dependency", 424 => "Method Failure", 425 => "Unordered Collection", 426 => "Upgrade Required", 428 => "Precondition Required", 429 => "Too Many Requests", 431 => "Request Header Fields Too Large", 444 => "No Response", 449 => "Retry With", 450 => "Blocked by Windows Parental Controls", 451 => "Unavailable For Legal Reasons", 494 => "Request Header Too Large", 495 => "Cert Error", 496 => "No Cert", 497 => "HTTP to HTTPS", 499 => "Client Closed Request", 
        500 => "Internal Server Error", 501 => "Not Implemented", 502 => "Bad Gateway", 503 => "Service Unavailable", 504 => "Gateway Timeout", 505 => "HTTP Version Not Supported", 506 => "Variant Also Negotiates", 507 => "Insufficient Storage", 508 => "Loop Detected", 509 => "Bandwidth Limit Exceeded", 510 => "Not Extended", 511 => "Network Authentication Required", 598 => "Network read timeout error", 599 => "Network connect timeout error"
        );
    
    /**
     * Envoi une erreur http avec le message d'erreur
     * @param int    $httpCode
     * @param string $error
     */
    protected function sendHttpError($httpCode=0, $error="")
    {
        $httpCode = ( isset(self::$httpStatusCodes[$httpCode]) ? $httpCode : 500 );
        if( !headers_sent() ) {
            header($_SERVER['SERVER_PROTOCOL']." ".$httpCode." ".self::$httpStatusCodes[$httpCode], true, $httpCode);
        }
        if( !empty($error) ) {
            $this->writeToStandardOutput($error);
        }
    }
    
    /**
     * Envoi le header avec le report des informations reçues par curl
     * Retourne true si le header de code 200 a été envoyé, false sinon (cas où il a déjà été envoyé)
     * @param array  $info   curl info
     * @return bool
     */
    protected function sendHttpHeader($info)
    {
        if( headers_sent() ) {
            return false;
        }
        
        $httpCode    = ( isset($info["http_code"]) ? $info["http_code"] : 200 );
        $contentType = ( isset($info["content_type"]) ? $info["content_type"] : "text/html; charset=UTF-8" );
        
        if( substr($contentType, 0, 9) == "text/html" ) {
            // les retours attendus sont soit : json, xml ou images
            header($_SERVER['SERVER_PROTOCOL']." 500 ".self::$httpStatusCodes[500], true, 500);
            return false;
        }
        
        $fileName    = md5($info["url"]);
        $fileExt     = explode('/', explode(";", $contentType)[0] )[1];
        
        header(sprintf("%s %s %s", $_SERVER['SERVER_PROTOCOL'], $httpCode, self::$httpStatusCodes[$httpCode]), true, $httpCode);
        header(sprintf("content-type: %s", $contentType));
        //header("Transfer-Encoding: chunked", true);
        //header("Content-Transfer-Encoding: binary", true); 
        //header(sprintf("Content-Disposition: attachment; filename=\"%s.%s\"", $fileName, $fileExt), true);
        /*
        header("Expires: 0", true);
        header( 'Cache-Control: no-store, no-cache, must-revalidate', true );
        header( 'Cache-Control: post-check=0, pre-check=0', true );
        header( 'Pragma: no-cache', true ); */
        return true;
    }
    
    /**
     * Envoi sur la sortie standard, un contenant au format json
     * @param array $data   données à envoyer
     * @param callback for jsonp response
     */
    protected function sendJsonResponse($data, $callback=null)
    {
        $httpCode = 200;
        
        $sData = json_encode($data);
        if($callback && $callback!=""){
            $sData = $callback."(".$sData.")";
        }
        
        header(sprintf("%s %s %s", $_SERVER['SERVER_PROTOCOL'], $httpCode, self::$httpStatusCodes[$httpCode]), true, $httpCode);
        header('Content-Type: application/json');
        header(sprintf('Content-length: %d', strlen($sData)));
        header("Expires: 0", true);
        header( 'Cache-Control: no-store, no-cache, must-revalidate', true );
        header( 'Cache-Control: post-check=0, pre-check=0', true );
        header( 'Pragma: no-cache', true );
        echo $sData;
    }
}
