<?php

namespace Visualiseur\Core\Controllers\Traits;

/**
 * RequestTrait
 * Complément aux méthodes fournies par Request
 */
Trait RequestTrait
{
    /**
     * Retourne tous les paramètres GET de l'url courrante
     * @return array
     */
    protected function getAllParamsGet()
    {
        return $_GET;
    }
  
    /**
     * Retourne tous les paramètres POST de l'url courrante
     * @return array
     */
    protected function getAllParamsPOST()
    {
        return $_POST;
    }
  
    /**
     * Retourne tous les paramètres situés GET ou POST en fonction de la méthode utilisée
     * @return array
     */
    protected function getRequestAllParamsByCurrentMethod()
    {
        return ( $this->request->isGet() ? $this->getAllParamsGet() : $this->getAllParamsPOST() );
    }
  
    /**
     * Retourne la partie protocole et nom de domaine de l'adresse url courante
     * @return string
     */
    protected function getRequestSchemeAndHttpHost()
    {
        return $this->request->getScheme().'://'.$this->request->getHttpHost();
    }
    
    /**
     * Retourne vrai si l'url possède un paramètre $name possède la valeur recherchée
     * Le paramètre est recherché selon 3 méthodes : majuscules, minuscules ou minuscules avec majuscule
     * @param string $name   nom du paramètre
     * @param string $value  valeur recherchée
     * @return bool
     */
    protected function hasRequestParamWithValue($name, $value="")
    {
        $upperParam   = strtoupper($name);
        $lowerParam   = strtolower($name);
        $ucfirstParam = ucfirst($lowerParam);

        $lvalue = strtolower($value);

        return ( $this->request->has($upperParam)
                 ? ( strtolower($this->request->get($upperParam)) == $lvalue ? true : false)
                 : ( $this->request->has($lowerParam)
                     ? ( strtolower($this->request->get($lowerParam)) == $lvalue ? true : false)
                     : ( $this->request->has($ucfirstParam)
                         ? ( strtolower($this->request->get($ucfirstParam)) == $lvalue ? true : false)
                         : false )));
    }
    
    /**
     * Retourne la valeur du paramètre $name, retourne chaine vide si non trouvée
     * Le paramètre est recherché selon 3 méthodes : majuscules, minuscules ou minuscules avec majuscule
     * @param string $name   nom du paramètre
     * @return string
     */
    protected function getRequestParam($name)
    {
        $upperParam   = strtoupper($name);
        $lowerParam   = strtolower($name);
        $ucfirstParam = ucfirst($lowerParam);

        return ( $this->request->has($upperParam)
                 ? $this->request->get($upperParam)
                 : ( $this->request->has($lowerParam)
                     ? $this->request->get($lowerParam)
                     : ( $this->request->has($ucfirstParam)
                         ? $this->request->get($ucfirstParam)
                         : '' )));
    }
}
