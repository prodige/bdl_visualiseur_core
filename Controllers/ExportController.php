<?php

namespace Visualiseur\Core\Controllers;

use ErrorException;
use Exception;
use InvalidArgumentException;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Phalcon\Http\Response;

/**
 * Class ExportController
 *
 * @package Visualiseur\Core\Controllers
 */
class ExportController extends ControllerBase
{

    public function jsonToExcelAction()
    {
        $this->exportJson("excel");
    }

    public function jsonToCsvAction()
    {
        $this->exportJson("csv");
    }

    public function jsonToShpAction()
    {
        $this->exportJson("shp");
    }

    public function exportCsvAction()
    {
        return $this->exportCsv();
    }

    public function exportXlsAction()
    {
        $response = new Response();

        if ($this->request->isOptions()) {
            $response->setStatusCode(200);
            return $response;
        }
        $this->transformToCsv();
        return $this->exportXls();
    }

    private function exportXls()
    {
        $response = new Response();
        try{
            $map = $this->request->getQuery('map');

            //$xls_data = $this->transformToXls($this->getCsvFilename());
            $file = $this->transformToXls($this->getCsvFilename());

            //$response->setHeader("Content-Type",'application/xls');
            //$response->setHeader('Content-Disposition',"attachment; filename='".$file."'");

            header('Content-Type:application/xls');
            header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition:attachment; filename='".$map.".xlsx'");
            header('Pragma: no-cache');
            //$response->setContent($xls_data);
            readfile($file);
            exit();

        }catch (\Exception $e) {
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to export excel/xlsx. " . $e->getMessage(),
            ]);
            return $response;
        }
        return $response;
    }

    private function transformToCsv()
    {
        $method='POST';
        $csv_output_format = '  
    OUTPUTFORMAT 
        NAME "CSV" 
        DRIVER "OGR/CSV" 
        MIMETYPE "text/csv" 
        FORMATOPTION "LCO:GEOMETRY=NONE" 
        FORMATOPTION "STORAGE=memory" 
        FORMATOPTION "FORM=simple" 
        FORMATOPTION "FILENAME=result.csv" 
    END
    ';

        $request= $this->request->getJsonRawBody();
        $layerName = $request->layer;
        $fields = implode(',',$request->fields);
        $map = $request->map;
        $wfsRequest= str_replace("geojson", "csv", $request->wfsRequest);

        if($map == null) 
        {
            throw new InvalidArgumentException("map parameter is required");
        }

        //Get mapfile
        $mapfile_path = realpath( $this->getPathToMapfileDirectory() . "/" . $map . ".map" ) ;

        if(!$mapfile_path)
        {
            throw new InvalidArgumentException(" Map file ". $map . ".map does not exist");
        }
        //Read mapfile
        $mapfile = fopen($mapfile_path,"r");
        $contents = fread($mapfile, filesize($mapfile_path));
        fclose($mapfile);

        //Manipulate contents
        //Add outputformat
        $insert_pos = strpos($contents,"OUTPUTFORMAT");
        $new_contents = substr($contents,0,$insert_pos) . $csv_output_format . substr($contents,$insert_pos);

        //Create temporary new mapfile
        $tmp_mapfile_path = $this->getPathToMapfileDirectory() . "/" . $map ."_tmp".".map";

        $mapfile_tmp = fopen($tmp_mapfile_path,"w+");
        fwrite($mapfile_tmp,$new_contents);
        fclose($mapfile_tmp);

        //initiate tmp mapfile mapscript
        $oMap = ms_newMapObj($tmp_mapfile_path);

        if($oMap){
            if($oLayer = $oMap->getLayerByName($layerName)){
                //TODO do it more safely getting info and not doing replace
                $oLayer->setMetadata("gml_include_items", $fields);
            }
        }
        $oMap->save($tmp_mapfile_path);
        chmod($tmp_mapfile_path, 0666);

        //Option Curl
        $url = $this->config->params->PRODIGE_URL_DATACARTO . '/map/' . $map ."_tmp";
        $payload = $wfsRequest;
        $headers = [
            "Content-Type: application/xml",
            "Accept: application/csv",
            'Content-Length: ' . strlen($payload)
        ];

        $csv_filename = $this->getPathToMapfileDirectory() . "/temp/" . str_replace("/","_",$map) .".csv";
        
        return $this->doCurlCallFile($csv_filename,$method,$url,$payload,$headers);
    }

    private function getCsvFilename()
    {
        return $this->getPathToMapfileDirectory() . "/temp/" . str_replace("/","_",$this->request->getJsonRawBody()->map) .".csv";
    }

    /**
     * @return string xls file name
     */
    private function transformToXls($filename_csv)
    {
        if(!file_exists($filename_csv))
        {
            throw new Exception("File ".$filename_csv." does not exist");
        }

        $filename_xls = str_replace(".csv",".xlsx",$filename_csv);

        $cmd = $this->config->params->CMD_OGR2OGR . ' -lco ENCODING="UTF-8" -f "XLSX" '.$filename_xls.' '.$filename_csv;

        $output = "";
        $result_code = "";
        exec($cmd,$output,$result_code);

        return $filename_xls;
        //return file_get_contents($filename_xls);
    }

    private function exportCsv()
    {
        $response = new Response();
        
        if ($this->request->isOptions()) {
            $response->setStatusCode(200);
            return $response;
        }
        try{
            $map = $this->request->getQuery('map');
            
            $file = $this->transformToCsv();

            //$response->setHeader("Content-Type",'application/csv');
            //$response->setHeader('Content-Disposition',"attachment; filename='".$file."'");

            header('Content-Type:application/csv');
            header("Content-Disposition:attachment; filename='".$map.".csv'");
            header('Pragma: no-cache');
            readfile($file);
            exit();

        } catch (\Exception $e) {
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to export excel/csv. " . $e->getMessage(),
            ]);
            return $response;
        }
        return $response;
    }

    protected function exportJson($outputFormat)
    {
        $response = new Response();
        try {

            $columns = array();
            $data = json_decode($this->request->getRawBody(), true);

            if (!$this->request->isPost()) {
                $response->setJsonContent([
                    "success" => false,
                    "message" => "Post Request is mandatory.",
                ]);
                return $response;
            }


            if (is_null($data) || !is_array($data)) {
                throw new \Exception("Failed to parse JSON. Content length is " . strlen($this->request->getRawBody()));
            }

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            if (is_array($data) && count($data) > 0 && array_key_exists(0, $data)) {
                foreach ($data[0] as $key => $value) {
                    array_push($columns, $key);
                }
            }

            foreach ($columns as $key => $column) {
                $sheet->setCellValueByColumnAndRow($key + 1, 1, $column);
            }

            $line = 1;
            foreach ($data as $element) {
                $line++;
                foreach ($columns as $key => $column) {
                    if (array_key_exists($column, $element)) {

                        if (is_array($element[$column])) {
                            $element[$column] = implode(",", $element[$column]);
                        }
                        $sheet->setCellValueByColumnAndRow($key + 1, $line, str_replace('\"', '"', $element[$column]));
                        if (strcmp(gettype($element[$column]), "boolean") == 0) {
                            if (strcmp($outputFormat, "csv") == 0) {
                                if ($element[$column]) {
                                    $forcedValue = "true";
                                } else {
                                    $forcedValue = "false";
                                }
                                $sheet->setCellValueExplicitByColumnAndRow($key + 1, $line, $forcedValue, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                            } else {
                                $sheet->getStyleByColumnAndRow($key + 1, $line)->getNumberFormat()->setFormatCode("BOOLEAN");
                            }
                        }
                    }
                }
            }

            if (strcmp($outputFormat, "excel") == 0) {
                $writer = new Xlsx($spreadsheet);

                header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                header("Content-Disposition: attachment; filename=" . "export-" . date("Ymd-His") . ".xlsx");

                $writer->save('php://output');
                die();
            } else if (strcmp($outputFormat, "csv") == 0) {
                $writer = new Csv($spreadsheet);
                $writer->setUseBOM(true); //UTF-8

                $writer->setLineEnding("\r\n");


                header("Content-Type: text/csv");
                header("Content-Disposition: attachment; filename=" . "export-" . date("Ymd-His") . ".xlsx");

                $writer->save('php://output');
                die();
            }
            else if( $outputFormat == 'shp' ){
                header("Content-Type: application/zip");
                header("Content-Disposition: attachment; filename=" . "export-" . date("Ymd-His") . ".zip");

                // génération du nom de fichier temporaire
                $filename = "export-" . date("Ymd-His")."_".rand(0, 9999);
                $geojsonFile = $filename.".geojson";
                $shpFile = $filename.".shp";
                $zipFile = $filename.".zip";
                
                // écriture du fichier
                file_put_contents($geojsonFile, json_encode($data));
                
                // convertion avec ogr2ogr en shpafile
                $retour = [];
                $cmd = " ogr2ogr -F 'ESRI Shapefile' ".$shpFile." ".$geojsonFile;
                exec($cmd);
                
                // supression du geojson temporaire
                $cmd = "rm ".$geojsonFile;
                exec($cmd);

                // on rassemble les fichier shp dans une archive zip
                $cmd ="zip ".$zipFile." ".$filename.".*";
                exec($cmd);
                
                // chargement du sphapefile
                $shapeFile = file_get_contents($zipFile);
                
                // supression des fichiers temporaire
                $retour = [];
                $cmd = "rm ".$filename.".*";
                exec($cmd);
                
                // envoie du fichier au navigateur
                file_put_contents('php://output', $shapeFile);

                die();
            }
            throw new \Exception("unknown output format.");
        } catch (\Exception $e) {
            $response->setJsonContent([
                "success" => false,
                "message" => "Failed to export excel/csv. " . $e->getMessage(),
            ]);
            return $response;
        }
    }

    public function getSpreadsheetType($var)
    {
        switch (gettype($var)) {
            case "integer":
            case "double":
                return \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC;
                break;
            case "boolean":
                return \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_BOOL;
                break;
            case "string":
            default:
                return \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING;
                break;
        }
    }

    /**
     * @return string filename
     */
    protected function doCurlCallFile($file,$method="GET",$url="",$payload="",$headers=[])
    {
        $ch = curl_init($url);
        $success = curl_setopt($ch, CURLOPT_URL, $url);
        if($method == "POST")
        {
            $success = $success 
            && curl_setopt($ch,CURLOPT_POST,true);
            //&& curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if(!empty($payload))
            {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            }
        }

        $success = $success
        && curl_setopt($ch, CURLOPT_HTTPHEADER, $headers)
        && curl_setopt($ch, CURLOPT_HEADER, false)
        && curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false)
        && curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        $fp = fopen($file,'w');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        
        $success = curl_exec($ch);
        fclose($fp);
        if(!$success){
            throw new \Exception("Error occured while executing curl call");
        }

        return $file;
    }

    /**
     * @return array data 
     */
    protected function doCurlCall($method="GET",$url="",$payload="",$headers=[])
    {
        $ch = curl_init($url);
        $success = curl_setopt($ch, CURLOPT_URL, $url);
        if($method == "POST")
        {
            $success = $success 
            && curl_setopt($ch,CURLOPT_POST,true)
            && curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            if(!empty($payload))
            {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            }
        }

        $success = $success
        && curl_setopt($ch, CURLOPT_HTTPHEADER, $headers)
        && curl_setopt($ch, CURLOPT_HEADER, false)
        && curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false)
        && curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        $data = curl_exec($ch);

        if(!($success && $data) || (curl_getinfo($ch)["content_type"]=="text/html;charset=UTF-8"))
        {
            throw new ErrorException("Exception in curl request ".json_encode(curl_getinfo($ch))." ".$data );
        }

        return $data;

    }
}
