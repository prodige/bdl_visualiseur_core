<?php
use Visualiseur\Core\Plugins\ParamsToArray;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/** @var Phalcon\Di $di */
$di->setShared('dispatcher', function () {
    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setEventsManager($this->getShared('eventsManager'));
    $dispatcher->setDefaultNamespace('Visualiseur\Core\Controllers');
    $dispatcher->getEventsManager()->attach(
        "dispatch:beforeDispatchLoop",
        new ParamsToArray()
    );
    return $dispatcher;
});

$di->setShared('view', function () {
    $view = new \Phalcon\Mvc\View();
    $view->setViewsDir(\Visualiseur\Core\Module::PATH . '/Views/');
    // $view->setViewsDir(\Visualiseur\Core\Module::PATH . '/Views/Default/scripts/')
    //     ->registerEngines([
    //         ".volt" => 'voltService',
    //     ]);
    return $view;
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash([
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ]);
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});
