<?php
return [
  'application' => [
    'controllersDir' => \Visualiseur\Core\Module::PATH . '/Controllers/',
    // 'viewsDir'       => \Visualiseur\Core\Module::PATH . '/Views/Default/scripts/',
    'viewsDir'       => \Visualiseur\Core\Module::PATH . '/Views/',
  ]
];